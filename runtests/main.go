package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

var (
	wg       = new(sync.WaitGroup)
	exitCode int32
	args     = [][]string{{"build"}, {"vet"}, {"test", "-race"}}
	tags     = []string{"estd_env_logdisabled", "estd_nodeps", ""}
)

func main() {
	ch := make(chan string, len(args)*len(tags))
	for _, tag := range tags {
		for _, args := range args {
			wg.Add(1)
			args, tag := args, tag
			go func() {
				defer wg.Done()
				ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
				defer cancel()
				args = append(args, fmt.Sprintf("-tags='%s'", tag))
				wd, _ := os.Getwd()
				if strings.Contains(wd, "runtests") {
					wd = filepath.Dir(wd)
				}
				cmd := exec.CommandContext(ctx, "go", args...)
				cmd.Dir = wd
				args = append(args, "./...")
				output, err := exec.CommandContext(ctx, "go", args...).CombinedOutput()
				if err != nil {
					atomic.AddInt32(&exitCode, 1)
					ch <- fmt.Sprintf("go %v => FAILED: %s", strings.Join(args, " "), output)
				} else {
					ch <- fmt.Sprintf("go %v => OK", strings.Join(args, " "))
				}
			}()
		}
	}
	var collected []string
	for i := 0; i < len(args)*len(tags); i++ {
		collected = append(collected, <-ch)
	}
	sort.Strings(collected)
	for _, c := range collected {
		log.Println(c)
	}
	wg.Wait()
	os.Exit(int(atomic.LoadInt32(&exitCode)))
}

/*
func main() {
	exec.Command("go", "build", t)
		echo -n "go build -tags='$1'" && go build -tags=$1 ./... && echo "  ~ok" || (echo "failed" && exit 1)
		echo -n "go vet -tags='$1' ./..." && go vet -tags="$1" ./... && echo "  ok" || (echo "failed" && exit 1)
		echo -n "go test -race -tags='$1' " && ! go test -race -tags=$1 ./... | grep -v "^ok" | grep -v "^?" && echo "  ok" || (echo "failed" && exit 1)
		echo "---------------------------------"
	}
	go version
	go mod tidy
	echo "---------------------------------"
		echo -n "go build -tags='$1'" && go build -tags=$1 ./... && echo "  ~ok" || (echo "failed" && exit 1)
		echo -n "go vet -tags='$1' ./..." && go vet -tags="$1" ./... && echo "  ok" || (echo "failed" && exit 1)
		echo -n "go test -race -tags='$1' " && ! go test -race -tags=$1 ./... | grep -v "^ok" | grep -v "^?" && echo "  ok" || (echo "failed" && exit 1)
		echo "----------------
	testtag estd_env_logdisabled
	testtag estd_nodeps
	testtag ""

}
*/
