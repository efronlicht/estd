module gitlab.com/efronlicht/estd

go 1.19

require (
	github.com/alicebob/miniredis/v2 v2.23.0
	github.com/google/uuid v1.3.0
	github.com/rs/zerolog v1.28.0
	go.uber.org/zap v1.23.0
	golang.org/x/exp v0.0.0-20221012211006-4de253d81b95
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/yuin/gopher-lua v0.0.0-20220504180219-658193537a64 // indirect
)

require (
	github.com/benbjohnson/clock v1.3.0 // indirect
	github.com/go-redis/redis/v9 v9.0.0-rc.1
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
)
