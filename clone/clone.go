package clone

import (
	"golang.org/x/exp/constraints"
)

/*
// Deep recursively clones an item of any kind. This uses reflection to traverse lists, etc.
// It will panic if it hits a loop. Don't use this in prod.
func Deep[V any](src V) (dst V) {
	return deep(reflect.ValueOf(src), 0).Interface().(V)
}
*/
// Slice shallowly clones a slice. The new slice is never nil.
func Slice[T Copy](src []T) []T {
	dst := make([]T, len(src))
	copy(dst, src)
	return dst
}

// Map shallowly clones a map. The returned map is never nil.
func Map[K, V Copy](src map[K]V) map[K]V {
	dst := make(map[K]V, len(src))
	for k, v := range src {
		dst[k] = v
	}
	return dst
}

// Copy types are primitives and copied during assignment.
type Copy interface {
	~string | constraints.Complex | constraints.Integer | ~bool | constraints.Float
}

/*
func deep(src reflect.Value, depth int) reflect.Value {
	const maxDepth = 20
	if depth > maxDepth {
		panic(fmt.Errorf("deep copy: depth > max depth! (%02d)!", maxDepth))
	}
	n := depth + 1
	v := reflect.ValueOf(src)
	t := v.Type()
	switch kind := v.Kind(); kind {
	case reflect.Func, reflect.Bool, reflect.String,
		reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Complex64, reflect.Complex128:
		return v
	case reflect.Pointer:
		p := reflect.New(v.Elem().Type())
		p.Set(deep(v.Elem(), n))
		return p
	case reflect.Slice:
		a := reflect.MakeSlice(t, v.Len(), v.Cap())
		for i := 0; i < v.Len(); i++ {
			a.Index(i).Set(v.Index(i))
		}
		return a
	case reflect.Array:
		pa := reflect.New(t)
		a := pa.Elem()
		for i := 0; i < v.Len(); i++ {
			c := deep(v.Index(i), n)
			a.Index(i).Set(c)
		}
		return a
	case reflect.Map:
		m := reflect.MakeMap(t)
		it := m.MapRange()
		for it.Next() {
			k, v := deep(it.Key(), n), deep(it.Value(), n)
			m.SetMapIndex(k, v)
		}
		return m
	case reflect.Struct:
		pa := reflect.New(t)
		a := pa.Elem()
		for i := 0; i < a.NumField(); i++ {
			a.Field(i).Set(deep(v.Field(i), 0))
		}
		return a

	default:
		panic(fmt.Errorf("unsupported value: type %s is of unsupported kind %s", t, kind))

	}
}

*/
