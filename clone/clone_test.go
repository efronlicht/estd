package clone_test

import (
	"testing"

	"gitlab.com/efronlicht/estd/clone"
	"gitlab.com/efronlicht/estd/equal"
)

func TestSlice(t *testing.T) {
	if !equal.Slice(clone.Slice([]string{"foo", "bar"}), []string{"foo", "bar"}) {
		t.Fail()
	}
}

func TestMap(t *testing.T) {
	if !equal.Map(clone.Map(map[string]string{"foo": "bar"}), map[string]string{"foo": "bar"}) {
		t.Fail()
	}
}
