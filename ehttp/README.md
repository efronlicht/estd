# ehttp

handle http requests in a natural way

## the big idea

```go
import "gitlab.com/efronlicht/estd/ehttp"
type Match struct {
    line int
    text string
}
// POST /grep
// req: {
//    pattern <string>
//    text: <string>
//    literal: <bool>
// }
// resp:
//   [ {line <int>, match: string}]
type Request struct {
    Pattern, Text string
    Literal bool
}

func main() {
    http.ListenAndServe(ehttp.JSONHandler(grep))
}
func grep(ctx context.Context, r Request) (matches []Match, err error){
    if ctx.Err() {
        return nil, err
    }
    if literal {
        for line, text in strings.Split(r.Text) {
            if strings.Contains(text, pattern) {
                matches = append(matches, Match{line, text})
            }
        }
        return matches
    }
    re, err := regex.Compile(pattern)
    if err != nil {
        return nil, ErrorWithStatus(fmt.Errorf("invalid regex pattern %q: %w", pattern, err), http.StatusBadRequest)
    }
    for line, text in strings.Split(lines) {
        if re.MatchString(text) {
            matches = append(matches, Match{line, text})
        }
    }
    return matches
}
```