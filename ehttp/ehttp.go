package ehttp

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

func writeErr(w http.ResponseWriter, err error, defaultStatus int) {
	status := defaultStatus
	if sc, ok := err.(interface{ StatusCode() int }); ok {
		status = sc.StatusCode()
	}
	w.WriteHeader(status)
	if wt, ok := err.(interface{ io.WriterTo }); ok {
		_, _ = wt.WriteTo(w)
	} else {
		w.Write([]byte(http.StatusText(status)))
	}
}

func writeSuccess(w http.ResponseWriter, v any) {
	status := http.StatusOK
	if sc, ok := v.(interface{ StatusCode() int }); ok {
		status = sc.StatusCode()
	}
	w.WriteHeader(status)
	if wt, ok := v.(interface{ io.WriterTo }); ok {
		_, _ = wt.WriteTo(w)
	} else {
		w.Write([]byte(http.StatusText(status)))
	}
}

// BuildHandler converts an ordinary function to a http.Handlerfunc, calling WriteSuccess(w, t) if there's no error and WriteError(w, err) if there is.
func BuildHandler[T any](f func(*http.Request) (t T, err error)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		resp, err := f(r)
		if err != nil {
			writeErr(w, err, http.StatusInternalServerError)
		}
		writeSuccess(w, resp)
	}
}

type JSON[T any] struct{ Inner T }

func (j JSON[T]) WriterTo(w io.Writer) (int64, error) {
	b, err := json.Marshal(j.Inner)
	if err != nil {
		return 0, fmt.Errorf("failed to marshal JSON: %w", err)
	}
	n, err := w.Write(b)
	return int64(n), err
}

func BodyAsJSON[T any](r *http.Request) (t T, err error) {
	err = json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		return t, fmt.Errorf("failed to decode request body as JSON: could not unmarshal to a %T: %w", t, err)
	}
	return t, nil
}

func Handler[I, O any](
	fromRequest func(*http.Request) (input I, err error),
	handle func(context.Context, I) (output O, err error),
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		input, err := fromRequest(r)
		if err != nil {
			writeErr(w, err, http.StatusBadRequest)
			return
		}
		output, err := handle(r.Context(), input)
		if err != nil {
			writeErr(w, err, http.StatusInternalServerError)
			return
		}
		writeSuccess(w, output)
	}
}

// FromBodyASJSON gets a T from the request body by calling  json.NewDecoder.Decode.
func FromBodyAsJSON[T any](r *http.Request) (T, error) {
	pt := new(T)
	err := json.NewDecoder(r.Body).Decode(&pt)
	return *pt, err
}

// WriteResponse writes resp to the ResponseWriter.
func WriteResponse(w http.ResponseWriter, resp Response) (int64, error) {
	w.WriteHeader(resp.StatusCode())
	return resp.WriteTo(w)
}

type Status int

func (s Status) StatusCode() int { return int(s) }

type Response interface {
	// StatusCode like like http.StatusOK or http.InternalServerError.
	StatusCode() int
	io.WriterTo
}

// StatusCode is a sensible default.
// If v implements interface{StatusCode() int}, or is an error that wraps something that does,
// Otherwise, if it's an error, use i
func StatusCode(v any) int {
	sc, ok := v.(interface{ StatusCode() int })
	if ok {
		return sc.StatusCode()
	}
	if err, ok := v.(error); ok {
		if errors.As(err, &sc) {
			return sc.StatusCode()
		}

		return http.StatusInternalServerError
	}
	return http.StatusOK
}
