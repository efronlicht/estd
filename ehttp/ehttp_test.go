package ehttp_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/efronlicht/estd/ehttp"
)

func TestWriteReadme(t *testing.T) {
	t.Skip("YOU NEED TO WRITE A README!")
}

func postJSON(h http.Handler, v any) (status int, respBody string) {
	body := new(bytes.Buffer)
	json.NewEncoder(body).Encode(v)
	w := httptest.NewRecorder()
	h.ServeHTTP(w, httptest.NewRequest("POST", "/", body))
	return w.Code, w.Body.String()
}

func TestHandler(t *testing.T) {
	t.Parallel()
	if got, _ := postJSON(ehttp.BuildHandler(func(r *http.Request) (ehttp.Status, error) {
		return http.StatusCreated, nil
	}), nil); got != http.StatusCreated {
		t.Error("expected ", http.StatusCreated, "got ", got)
	}
	if got, _ := postJSON(ehttp.BuildHandler(func(r *http.Request) (any, error) {
		return 0, errors.New("some error")
	}), nil); got != http.StatusInternalServerError {
		t.Error("expected ", http.StatusCreated, "got ", http.StatusInternalServerError)
	}
}

func TestBuildHandler(t *testing.T) {
	type Request struct{ Foo string }
	h := ehttp.Handler(ehttp.FromBodyAsJSON[Request], func(ctx context.Context, r Request) (ehttp.Status, error) {
		if r.Foo == "foo" {
			return http.StatusOK, nil
		} else {
			return 0, errors.New("expected foo")
		}
	})
	for name, tt := range map[string]struct {
		body       any
		wantStatus int
	}{
		"OK":                           {Request{"foo"}, http.StatusOK},
		"Wrong Value":                  {Request{"bar"}, http.StatusInternalServerError},
		"failed to parse request body": {"a[", http.StatusBadRequest},
	} {
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			gotStatus, _ := postJSON(h, tt.body)
			if gotStatus != tt.wantStatus {
				t.Fatalf("expected %d, but got %d", tt.wantStatus, gotStatus)
			}
		})
	}
}
