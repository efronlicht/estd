// srv contains server middleware.
package srv

import (
	"net/http"

	"gitlab.com/efronlicht/estd/ectx"
	"gitlab.com/efronlicht/estd/parse"
)

func ignoreErr[T any](t T, err error) T { return t }
func AddTraceFromHeaderToRequestContext(h http.Handler, label string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		trace, _ := parse.Trace(r.Header.Get("Estd-Trace"))
		r = r.WithContext(ectx.WithVal(r.Context(), trace))
		h.ServeHTTP(w, r)
	}
}
