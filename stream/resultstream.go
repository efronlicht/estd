package stream

import (
	"context"
	"errors"
	"fmt"
	"runtime"
	"strings"
	"sync"
	"sync/atomic"
)

type Result[T, R any] struct {
	In  T
	Out R
	Err error
}

// Results is a read-only channel of the results of a mapping operation.
type Results[T, R any] <-chan *Result[T, R]

// CollectN collects at most N elements from the result stream. Termination:
func (rs Results[T, R]) CollectN(n int) (out []R, err error) {
	var errs []string
	out = make([]R, cap(rs))
	for r := range rs {
		if r == nil {
			errs = append(errs, "nil result")
		}
		if r.Err != nil {
			errs = append(errs, err.Error())
		} else {
			out = append(out, r.Out)
		}
	}
	return out, resultErrs(errs)
}

// CollectNCtx elements from the ResultStream until one of these happens:
//   - The ResultStream is closed.
//   - N elements have been obtained.
//   - The context has been cancelled.
//
// Note that this allocates a slice of [R] of capacity N.
func (rs Results[T, R]) CollectNCtx(ctx context.Context, n int) (out []R, err error) {
	var errs []string
	out = make([]R, 0, n)
	for ; n > 0; n-- {
		select {
		case <-ctx.Done():
			return out, fmt.Errorf("%w: %v", err, resultErrs(errs))
		case r, ok := <-rs:
			if !ok {
				return out, fmt.Errorf("%d errors: %v", len(errs), strings.Join(errs, ";"))
			}
			if r == nil {
				errs = append(errs, "nil result")
			}
			out = append(out, r.Out)
		}
	}
	return out, resultErrs(errs)
}

func resultErrs(errs []string) error {
	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return fmt.Errorf("%d errors: %v", len(errs), strings.Join(errs, ";"))
	}
}

// ChanToSlice collects the results of ch into a slice, returning when the channel is closed or the context is done, whichever comes first.
// Termination: this terminates if the context does.
func ChanToSlice[T any](ctx context.Context, ch <-chan T) ([]T, error) {
	a := make([]T, 0, cap(ch))
	for {
		select {
		case <-ctx.Done():
			return a, ctx.Err()
		case t, ok := <-ch:
			if !ok {
				return a, nil
			}
			a = append(a, t)
		}
	}
	// unreachable!
}

// ChanToSliceN collects at most N elements of ch into a slice, returning under the following conditions:
// - the channel is closed
// - the context is done
// - N elements have been collected
func ChanToSliceN[T any](ctx context.Context, ch <-chan T, n int) ([]T, error) {
	a := make([]T, 0, n)
	for ; n > 0; n-- {
		select {
		case <-ctx.Done():
			return a, ctx.Err()
		case t, ok := <-ch:
			if !ok {
				return a, nil
			}
			a = append(a, t)
		}
	}
	return a, nil
}

// SliceToChan converts a slice []T to a buffered channel of the same length.
func SliceToChan[T any](a []T) <-chan T {
	ch := make(chan T, len(a))
	for i := range a {
		ch <- a[i]
	}
	close(ch)
	return ch
}

// StreamMap executes "f" in parallel across the items piped to "in", creating a ResultStream.
// It will attempt to terminate early if the context is done, but "f" is responsible for respecting the context's cancellation,
// and the consumer(s) of the ResultStream must either consume the result entirely or drop it out of scope.
func StreamMap[T, R any](ctx context.Context, in chan T, f func(context.Context, T) (R, error)) Results[T, R] {
	wg := new(sync.WaitGroup)
	workers := runtime.GOMAXPROCS(0)
	wg.Add(workers)
	out := make(chan *Result[T, R], cap(in))
	for j := 0; j < workers; j++ {
		go func() {
			defer wg.Done()
			select {
			case <-ctx.Done():
				out <- &Result[T, R]{Err: ctx.Err()}
				return
			case t, ok := <-in:
				if !ok {
					return
				}
				r, err := f(ctx, t)
				out <- &Result[T, R]{In: t, Out: r, Err: err}
			}
		}()
	}
	return out
}

// ForkJoin executes a function 'f' in parallel across a slice T.
// The following guarantees hold:
//   - len(a) == len(r) == len(e)
//   - r[i], err[i] = f(a[i])
//
// `f` must be responsible for respecting the context's cancellation.
// ForkJoin terminates if and only if f(ctx, a[i]) does.
func ForkJoin[T, R any](ctx context.Context, a []T, f func(context.Context, T) (R, error)) (r []R, e []error) {
	n := len(a)
	task := int64(-1) // we add 1 before checking this
	next := func() int { return int(atomic.AddInt64(&task, 1)) }
	r, e = make([]R, n), make([]error, n)
	wg := new(sync.WaitGroup)
	workers := runtime.GOMAXPROCS(0)
	wg.Add(workers)
	// we could use a channel, but atomics are smaller and faster than locking.
	for j := 0; j < runtime.GOMAXPROCS(0); j++ {
		go func() {
			defer wg.Done()
			for i := next(); i < n; i = next() {
				r[i], e[i] = f(ctx, a[i])
			}
		}()
	}
	wg.Wait()
	return r, e
}
