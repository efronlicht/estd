package stream

import (
	"unsafe"
)

// AsReceiver downcasts a slice of bidirectional channels to receive-only channels.
// The returned slice points to the same underlying array: modify it at your peril!
func AsReceiver[T any](a []chan T) []<-chan T {
	return *(*[]<-chan T)(unsafe.Pointer(&a))
}

// AsSender downcasts a slice of bidirectional channels to send-only channels.
// The returned slice points to the same underlying array: modify it at your peril!
func AsSender[T any](a []chan T) []chan<- T {
	return *(*[]chan<- T)(unsafe.Pointer(&a))
}
