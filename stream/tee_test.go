package stream_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"gitlab.com/efronlicht/estd/equal"
	"gitlab.com/efronlicht/estd/stream"
	. "gitlab.com/efronlicht/estd/stream"
)

func asRecSlive[T any](ch <-chan T) []T {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	got, err := ChanToSlice(ctx, ch)
	if err != nil {
		panic(err)
	}
	return got
}

func asSlice[T any](ch chan T) []T {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	got, err := ChanToSlice(ctx, ch)
	if err != nil {
		panic(err)
	}
	return got
}

func TestTee(t *testing.T) {
	t.Parallel()
	want := asSlice(Range[int](5))
	L, R := Tee(context.Background(), Range[int](5))
	if gotL, gotR := asRecSlive(L), asRecSlive(R); !equal.Slice(want, gotL) || !equal.Slice(want, gotR) {
		t.Fatalf("expected L == R == want, but got %v, %v, %v", gotL, gotR, want)
	}
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	L, R = Tee(ctx, Range[int](5))
	if gotL, gotR := asRecSlive(L), asRecSlive(R); len(gotL) > 0 || len(gotR) > 0 {
		t.Fatalf("expected len(L) == len(R) == 0, but got %v, %v, %v", gotL, gotR, want)
	}
}

var chans []<-chan int

func TestTeeN(t *testing.T) {
	t.Parallel()
	for n := 1; n <= 1024; n *= 2 {
		n := n
		test := func(t *testing.T) {
			if testing.Short() && n > 32 {
				t.Skipf("SKIP %s: slow", t.Name())
			}
			t.Parallel()
			want, _ := ChanToSlice(context.Background(), Range[int](n))
			for _, ch := range stream.TeeN(context.Background(), Range[int](n), n) {
				got, _ := ChanToSlice(context.Background(), ch)
				if !equal.Slice(want, got) {
					t.Fatalf("expected %v, got %v", want, got)
				}
			}
		}
		t.Run(fmt.Sprint(n), test)
	}
}

func TestTeeNError(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	cancel()
	t.Parallel()
	for n := 1; n <= 1024; n *= 2 {
		n := n
		test := func(t *testing.T) {
			if testing.Short() && n > 32 {
				t.Skipf("SKIP %s: slow", t.Name())
			}
			t.Parallel()
			for _, ch := range stream.TeeN(ctx, Range[int](n), n) {
				got, _ := ChanToSlice(context.Background(), ch)
				// this test is nondeterministic, due to scheduling.
				if len(got) != 0 {
					t.Fatalf("expected len(got) == 0, but got %d", len(got))
				}
			}
		}
		t.Run(fmt.Sprint(n), test)
	}
}

func BenchmarkTee(b *testing.B) {
	b.ReportAllocs()
	b.Log("n = channels, m = items per channel")
	b.Run("partial drain", func(b *testing.B) {
		for m := 2; m <= 1024; m *= 8 {
			for n := 2; n <= 1024; n *= 8 {
				b.Run(fmt.Sprintf("channels=%02d_items_per_channel=%02d", n, m), func(b *testing.B) {
					b.StopTimer()
					for i := 0; i < b.N; i++ {
						start := time.Now()
						ch := Range[int](m)
						b.StartTimer()

						chans = stream.TeeN(context.Background(), ch, n)
						for i := range chans {
							for range chans[i] {
								// no-op
							}
						}
						b.StopTimer()
						ms := float64(time.Since(start).Nanoseconds())
						b.ReportMetric(ms/float64(m), "ns/item/op")
						b.ReportMetric(ms/float64(n), "ns/ch/op")
						b.ReportMetric(ms/(float64(n)*float64(m)), "ns/ch/item/op")
					}
				})
			}
		}
	})
	b.Run("full drain", func(b *testing.B) {
		for m := 2; m <= 1024; m *= 8 {
			for n := 2; n <= 1024; n *= 8 {
				b.Run(fmt.Sprintf("channels=%02d_items_per_channel=%02d", n, m), func(b *testing.B) {
					b.StopTimer()
					for i := 0; i < b.N; i++ {
						start := time.Now()
						ch := Range[int](m)
						b.StartTimer()

						chans = stream.TeeN(context.Background(), ch, n)
						for i := range chans {
							<-chans[i]
						}
						b.StopTimer()
						ms := float64(time.Since(start).Nanoseconds())
						b.ReportMetric(ms/float64(m), "ns/item/op")
						b.ReportMetric(ms/float64(n), "ns/ch/op")
						b.ReportMetric(ms/(float64(n)*float64(m)), "ns/ch/item/op")
					}
				})
			}
		}
	})
}
