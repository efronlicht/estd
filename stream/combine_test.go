package stream_test

import (
	"context"
	"sort"
	"testing"

	. "gitlab.com/efronlicht/estd/stream"
)

func TestCombine(t *testing.T) {
	a := make([]chan int, 3)
	for i := range a {
		a[i] = make(chan int, 10)
		for j := 0; j < 10; j++ {
			a[i] <- j*3 + i
		}
		close(a[i])
	}
	combined := CombineN(context.Background(), AsReceiver(a)...)
	got, _ := ChanToSlice(context.Background(), combined)
	sort.Ints(got)
	for i, n := range got {
		if got[i] != n {
			t.Fail()
		}
	}
}
