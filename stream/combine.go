package stream

import (
	"context"
	"sync"

	"gitlab.com/efronlicht/estd/emath"
	"golang.org/x/exp/constraints"

	"gitlab.com/efronlicht/estd/stream/internal/generated"
)

func CombineN[T any](ctx context.Context, src ...<-chan T) <-chan T {
	var c int
	for i := range src {
		c += cap(src[i])
	}
	dst := make(chan T, emath.Max(c, len(src)))
	go func() {
		CombineIntoN(ctx, dst, src...)
		close(dst)
	}()
	return dst
}

func Range[T constraints.Integer](n int) chan T {
	ch := make(chan T, n)
	for i := 0; i < n; i++ {
		ch <- T(i)
	}
	close(ch)
	return ch
}

// CombineIntoN feeds N input channels into a single output channel.
func CombineIntoN[T any](ctx context.Context, dst chan<- T, src ...<-chan T) (int, error) {
	const MAX = generated.MAX
	if len(src) <= MAX {
		return generated.CombineN(ctx, dst, src...)
	}
	var err error
	var mux sync.Mutex
	var wg sync.WaitGroup
	var n int
	for len(src) > 0 {
		i := emath.Min(MAX, len(src))
		sub := src[i:]
		src = src[:i]
		wg.Add(1)
		go func() {
			defer wg.Done()
			m, e := generated.CombineN(ctx, dst, sub...)
			mux.Lock()
			defer mux.Unlock()
			if err == nil && e != nil {
				err = e
			}
			n += m
		}()

	}
	wg.Wait()
	return n, err
}
