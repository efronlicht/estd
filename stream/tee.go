package stream

import (
	"context"
	"fmt"
	"sync"
	"unsafe"

	"gitlab.com/efronlicht/estd/emath"
	"gitlab.com/efronlicht/estd/stream/internal/generated"
)

// TeeTo is as Tee, but pipes to the provided channels instead of making it's own.
// It does NOT close the channel.
// Termination: this function terminates when L and R are drained or when the context is cancelled.
func TeeTo[T any](ctx context.Context, src <-chan T, L, R chan<- T) error {
	if err := ctx.Err(); err != nil {
		return fmt.Errorf("context cancelled before tee start: %w", err)
	}
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case t, ok := <-src:
			if !ok {
				return ctx.Err()
			}
			select {
			case <-ctx.Done():
				return ctx.Err()
			case L <- t:
				select {
				case <-ctx.Done():
					return ctx.Err()
				case R <- t:
					continue
				}
			case R <- t:
				select {
				case <-ctx.Done():
					return ctx.Err()
				case L <- t:
					continue
				}
			}
		}
	}
}

// TeeToN is as TeeN, but pipes to the provided channels instead of making it's own.
// Safety: this is considered to take ownership of the *slice* dst, which is not safe for use afterwards.
// For every read from src, TeeTooN writes a shallow copy to each channel in dst.
//
//	 Terminates when:
//		- the context is cancelled:
//		- src has been closed and every entry has been written to dst.
//
// Safety:
//
// Replacing dst[i] (with nil or with another channel) after a call to TeeToN is undefined behavior.
// Failing to drain any given chaanel may block the others.
// Be careful with pointers.
func TeeToN[T any](ctx context.Context, src <-chan T, dst []chan<- T) (n int, err error) {
	const MAX = generated.MAX
	if err := ctx.Err(); err != nil {
		return 0, fmt.Errorf("context closed before TeeToN started: %w", err)
	}

	if len(dst) > MAX*MAX {
		panic(fmt.Errorf("can't handle more than %d*%d (%d) destination channels", MAX, MAX, MAX*MAX))
	}
	if len(dst) <= MAX {
		return generated.TeeN(ctx, src, dst...)
	}
	mid := make([]chan T, 1+(len(dst)-1)/MAX)
	for i := range mid {
		mid[i] = make(chan T, cap(src))
	}
	go func() {
		_, _ = generated.TeeN(ctx, src, AsSender(mid)...)
		for i := range mid {
			close(mid[i])
		}
	}()
	wg := new(sync.WaitGroup)
	var mux sync.Mutex

	wg.Add(len(mid))
	k := 0
	for i := range mid {
		i, j := i, k
		k = emath.Min(k+MAX, len(dst))
		go func(i, j, k int) {
			defer wg.Done()
			m, e := generated.TeeN(ctx, mid[i], dst[j:k]...)
			mux.Lock()
			defer mux.Unlock()
			n += m
			if e != nil && err != nil {
				err = e
			}
		}(i, j, k)

	}
	wg.Wait()
	return n, err
}

// Tee immediately returns two channels, each of which outputs the identical input from src.
// That is, for every read from src, Tee writes a shallow copy to A and to B.
// Be careful with pointers.
// See TeeN for more than two copies.
func Tee[T any](ctx context.Context, src <-chan T) (a, b <-chan T) {
	L, R := make(chan T, cap(src)), make(chan T, cap(src))
	go func() {
		defer func() { close(L); close(R) }()
		TeeTo(ctx, src, L, R)
	}()
	return L, R
}

// TeeN is the generalized form of Tee. It returns N channels, each of which outputs the identical input from src.
// That is, for every read from src, Tee writes a shallow copy to A and to B.
// Be careful with pointers.
// See Tee for just two output channels.
func TeeN[T any](ctx context.Context, src <-chan T, n int) (copies []<-chan T) {
	dst := make([]chan T, n)

	for i := range dst {
		dst[i] = make(chan T, cap(src))
	}

	go func() {
		defer func() {
			for i := range dst {
				close(dst[i])
			}
		}()
		// cast safety: a send or receive-only channel is identical to a bidirectional channel at runtime: they're both just pointers to a *runtime.hchan:
		// github.com/golang/go/blob/4fc9565ffce91c4299903f7c17a275f0786734a1/src/runtime/chan.go#L17-L29.
		// we save an alloc by casting the type directly rather than having a new slice and copying over each pointer.
		TeeToN(ctx, src, *(*[]chan<- T)(unsafe.Pointer(&dst)))
	}()
	// cast safety: see above.
	return AsReceiver(dst)
}
