package erand_test

import (
	"testing"

	"gitlab.com/efronlicht/estd/emath"
	. "gitlab.com/efronlicht/estd/erand"
	"golang.org/x/exp/constraints"
)

func TestNew(t *testing.T) {
	t.Skipf("slow: generates a lot of random numbers, churning the CPU")
	type Point[T constraints.Integer] struct{ X, Y, Z T }

	rng := NewRNG()
	var cnt [0x0FF + 1]Point[uint64]
	var buf [0x000F_FFFF]Point[byte]
	for i := 0; i < 0xFF; i++ {
		for j := range buf {
			buf[j] = Point[byte]{0, 0, 0}
		}
		FillSliceRaw(buf[:], rng)
		for _, p := range buf {
			cnt[p.X].X++
			cnt[p.Y].Y++
			cnt[p.Z].Z++
		}
	}
	// law of large numbers: we expect each one to be within about 5% of the average.
	// this is not very rigorous: there's a lot more that goes into 'true randomness' than this.
	// but we're assuming we trust the underlying RNG: this is just making sure the logic seems more or less right.
	// We ran 255 trials, but there's 255 'slots': they should all be about the same.
	const expectedAverage = float64(0x000F_FFFF)
	for i, p := range cnt {
		for k, v := range map[string]uint64{
			"X": p.X,
			"Y": p.Y,
			"Z": p.Z,
		} {
			if !emath.WithinTol(expectedAverage, float64(v), emath.FloatTol{Rel: 0.05}) {
				t.Errorf("expected %d.%s to be ~ %v, but was %v", i, k, expectedAverage, v)
			}
		}
	}
}
