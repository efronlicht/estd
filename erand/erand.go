package erand

import (
	crand "crypto/rand"
	"math"
	"math/big"
	"math/rand"
	"unsafe"
)

func as[B, T any](t T) (b B) { return *(*B)(unsafe.Pointer(&t)) }

// FillSliceRaw fills the slice with randomly generated values of T. This is not safe for all types: see NewRaw.
func FillSliceRaw[T any](dst []T, rng rand.Source64) {
	for i := range dst {
		dst[i] = NewRaw[T](rng)
	}
}

// UpperAlphanumericString generates string of len-n from the characters [A-Z0-9], chosen at random.
func UpperAlphanumericString(rng *rand.Rand, n int) string {
	b := make([]byte, n)
	for i := range b {
		if m := byte(rng.Intn(36)); m < 26 {
			b[i] = 'A' + m
		} else {
			b[i] = '0' + (m - 26)
		}
	}
	return string(b)
}

// ChooseKWithReplacement chooses k not-necessarially-distinct elements from a bag of items.
func ChooseKWithReplacement[T any](rng *rand.Rand, bag []T, k int) []T {
	out := make([]T, k)
	for i := range out {
		out[i] = bag[rng.Intn(len(bag))]
	}
	return out
}

// SliceOf generates a random slice of length n filled with random numbers.
func SliceOf[T any](rng rand.Source64, len int) []T {
	dst := make([]T, len)
	FillSliceRaw(dst, rng)
	return dst
}

// NewRNG creates a new, NON-thread-safe, NON-cryptographically seucre rng, initializing the seed via crypto/rand.Int.
func NewRNG() *rand.Rand {
	seed, _ := crand.Int(crand.Reader, big.NewInt(math.MaxInt64))
	return rand.New(rand.NewSource(seed.Int64()))
}

// Pop a random element from the slice.
// Panics if the slice is empty.
func PopRandom[T any](rng interface{ Intn(int) int }, a []T) (remaining []T, popped T) {
	j := rng.Intn(len(a))
	b := len(a) - 1
	a[j], a[b] = a[b], a[j]
	return a[:b], a[b]
}

// ChooseKInPlace chooses N elements from the sample without replacement.
// This modifies the original slice and hands back a _sub-slice_. If this isn't what you want, copy the slice first.
func ChooseKInPlace[T any](rng interface{ Intn(int) int }, a []T, k int) []T {
	for i := 0; i < k; i++ {
		b := len(a) - (1 + i) // back
		j := rng.Intn(len(a))
		a[j], a[b] = a[b], a[j]
	}
	return a[:k]
}

// NewRaw generates a random element of T from completely random bits. This is only defined for the following types:
// non-string primitives, primitive integers, floats, complex numbers, booleans, and arrays and structs (recursively) comprised of those types.
// Floats and Complex numbers may not give a USEFUL result: subnormal numbers, NaNs, and +/- infinity may all result.
// That is, the following types are OK:
// int8..=int64, uint8..=uint64, bool, []int8..=int64, struct{x, y, z int}, [20]struct{x, y, z int}
func NewRaw[T any](rng rand.Source64) (t T) {
	n := unsafe.Sizeof(t)
	switch (n + 7) / 8 {
	default:
		panic("no implementation for n > 255")
	case 0:
		// zero-sized type: nothing to do.
		return t

	case 1:
		const cap = (1 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 2:
		const cap = (2 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 3:
		const cap = (3 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 4:
		const cap = (4 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 5:
		const cap = (5 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 6:
		const cap = (6 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 7:
		const cap = (7 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 8:
		const cap = (8 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 9:
		const cap = (9 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 10:
		const cap = (10 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 11:
		const cap = (11 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 12:
		const cap = (12 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 13:
		const cap = (13 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 14:
		const cap = (14 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 15:
		const cap = (15 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 16:
		const cap = (16 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 17:
		const cap = (17 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 18:
		const cap = (18 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 19:
		const cap = (19 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 20:
		const cap = (20 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 21:
		const cap = (21 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 22:
		const cap = (22 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 23:
		const cap = (23 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 24:
		const cap = (24 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 25:
		const cap = (25 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 26:
		const cap = (26 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 27:
		const cap = (27 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 28:
		const cap = (28 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 29:
		const cap = (29 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 30:
		const cap = (30 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)

	case 31:
		const cap = (31 + 7) / 8
		var buf [cap]uint64
		for i := range buf {
			buf[i] = rng.Uint64()
		}
		// zero out remaining bytes of the type.
		buf[cap-1] >>= 8 - (n % 8)
		return as[T](buf)
	}
}
