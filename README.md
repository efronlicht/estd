# estd: (efron's extended) standard library

This extended standard library is a grab-bag of missing tooling I've found myself reaching for or rewriting over and over in 5+ years as a golang dev, mostly in distributed systems.

## table of contents

- [estd: (efron's extended) standard library](#estd-efrons-extended-standard-library)
  - [table of contents](#table-of-contents)
  - [installation](#installation)
  - [testing](#testing)
    - [example](#example)
  - [principles](#principles)
    - [No bullshit](#no-bullshit)
    - [Use what you need](#use-what-you-need)
    - [Failure is inevitable, so design for failure](#failure-is-inevitable-so-design-for-failure)
    - [Be clear, not clever](#be-clear-not-clever)
    - [Be helpful, not nice](#be-helpful-not-nice)
  - [dependencies](#dependencies)
    - [dependencies - mandatory](#dependencies---mandatory)
    - [dependencies - optional](#dependencies---optional)
  - [packages](#packages)
  - [version policy](#version-policy)

## installation

```sh
#!/bin/sh
# navigate to the directory of your project, i.e, via
# cd ~/go/gitlab.com/efronlicht/myproject
go get estd@latest
```

## testing

the small program ./runtests runs the tests for each combination of tags.

### example

```bash
+testbash
go build    -tags=estd_env_logdisabled
go test     -tags=estd_env_logdisabled
ok
---------------------------------
go build    -tags=estd_nodeps
go test     -tags=estd_nodeps
ok
---------------------------------
go build    -tags=
go test     -tags=
ok
```

## principles

### No bullshit

- No frameworks.
- No god objects.
- No import-time behavior.
- No weasel words like 'best practices'.
- No YAML.

### Use what you need

- Disable dependencies through build tags.
- Functions over objects.

### Failure is inevitable, so design for failure

- Don't tie together too many parts of the system.
- Anything that _could_ fail needs to return an error.
- If it can stall, it needs a context.Context.
- If it can stall, it can fail.
- Almost anything can fail or stall.
- The operating system is the outside world, too.
- A library-spawned goroutine should never panic.

### Be clear, not clever

- A clear error message is worth a thousand words.
- APIs should be as simple as possible, but no simpler.
- Small performance sacrifices are worth it for simpler debugging.
- Generics over reflection.
- Use fewer words.

### Be helpful, not nice

## dependencies

Where possible, outside dependencies are designed to be _optional extra features_ that can be disabled through build tags.

Disable all optional dependencies with build tag `estd_nodeps`

### dependencies - mandatory

- github.com/google/uuid v1.3.0

### dependencies - optional

these can be disabled through the following build tags.
disable all optional dependencies with `estd_nodeps`

- go.uber.org/zap: `estd_nozap`
- github.com/rs/zerolgo: `estd_nozerolog`

## packages

- ### [**cache**](./cache)

  Cache function results, either locally or across instances.

- #### [**cache.InMem**](cache/inmem.go) in-program, in-memory cache with timed invalidation

- #### [**cache.Disk**](cache/disk.go) cache serialized to local filesystem w/ timed invalidation

- #### [**cache.Redis**](cache/redis.go) redis cache.

- ### [**codegen**](./codegen/): Code generation to support the rest of `estd`

- ### [**conv**](./conv/conv.go): Convert types
```go
k, v := SortedMapItems(map[string]string{"a": "foo", "b": "bar"})
for i := range keys {
  fmt.Printf("k[%d] = %s; v[%d] = %s\n", i,k [i],  i, v[i])
  // k[0] = a; v[0] = "foo"
  // k[1] = b; v[1] = "bar"
}
```
- ### [**ectx**](./ectx/extx.go): Helper functions for context.Context

  ```go
  // get a *zap.Logger from the context if it exists, calling zap.L() for the global logger otherwise
  log := ectx.ValOrElse(ctx, zap.L)
  ```
  
- ### [**enve**](./enve/README.md): Simple configuration through the environment

  ```go
  var publicKey = enve.MustGetString("MY_APP_PUBLIC_KEY")
  var port = enve.PortOr("MY_APP_PORT", 8080)
  ```
- ### [**eruntime**](./eruntime/eruntime.go) A more user-friendly alternative to the runtime package in the stdlib.
```go
  fmt.Printf("I'm running in %s", eruntime.ThisFile())
- ### [**lazy**](./lazy/lazy.go): lazily evaluated variables

- ### [**parse**](./parse/README.md):  Convenient parsing of `[]byte` and `string` with helpful error messages

  ```go
  ints, err := parse.Slice(strconv.Atoi, "88 73 90 40")
  ```

- ### [**containers**](./containers/README.md) Common data structures

- #### [**containers/esync**](./containers/esync) Concurrency-safe datastructures: generic wrappers for the stdlib's sync.Pool and sync.Map



- #### [**containers/set**](./containers/set/hset.go) Mathematical sets and their operations


- ##### **Hset[K]** Unordered set based off a hashmap


- ### [**deref**](./deref/deref.go) Utility functions for converting to and from pointer types and treating them as optional values. Often too clever. Use with caution

- ### [**emath**](./emath/math.go) Basic mathematical functions: Min, Max, Clamp, approximate equality, etc
- ### [**equal**](./equal/equal.go) Test equality of maps, slices, etc.
```go
   import "gitlab.com/efronlicht/estd/equal"
   import "strings"
   fmt.Println(equal.Slice([]string{"a", "b", "c"}, strings.Fields("a" "b" "c")))
   /// true
```
- ### [**eruntime**](./eruntime/eruntime.go): Observe the runtime and callstack and get metadata about functions, with hooks for the [zap](https://github.com/uber-go/zap) and [zerolog](https://github.com/rs/zerolog) loggers

- ### [**ehttp**](./ehttp/ehttp.go): Helper functions for http.Request and http.Response

- ### [**erand**](./erand/erand.go): Random value generation & choice from sets

  ```go
  type Pair struct{A, B string}
  func pairOffRandomly(people []string)(pairs []Pair, err error) {
    if len(people) % 2 != 0 {
      return errors.New("not an even number of people!")
    }
    rng := erand.NewRNG()
    for len(people) > 0 {
      var a, b T
      people, a = erand.PopRandom(rng, people)
      people, b = erand.PopRandom(rng, people)
      pairs = append(pairs, Pair{a, b})
    }
    return pairs
  ```

- ### [**ops**](./ops): Go's operators as generic functions

- ### [**reexport**](./reexport): Parts of the go source that weren't exported that I needed

- ### [**stream**](./stream): Streams and pipelines. Tools for fork-join parallelism and concurrent streams


- ### [**stringutil**](./stringutil): string-handling utilities which were missing from stdlib's `strings`

- ### [**trace**](./trace/trace.go): Trace execution across boundaries

- ### [**zaputil**](./zaputil/zaputil.go): Generics for performant structured logging using [zap]([go.uber.org/zap])

## version policy

This doesn't exist yet. I'll come up with something.
