package stringutil

import (
	"strings"
	"unicode/utf8"
)

//	// alias for strings.Cut
//
// Cut slices s around the first instance of sep, returning the text before and after sep. The found result reports whether sep appears in s. If sep does not appear in s, cut returns s, "", false.
//
//	if left, right, _ := Cut("my name is", " ")); left != "my", right != "name is" {
//	  panic("bad cut!")
//	}
func Cut(text, sep string) (left, right string, ok bool) {
	return strings.Cut(text, sep)
}

// CutAfter splits s immediately after the first occurence of sub ok is false if sub is not present. If sep does not appear in s, CutAfter returns s, "", false.
func CutAfter(s, sub string) (left, right string, ok bool) {
	i := strings.Index(s, sub)
	if i == -1 {
		return s, "", false
	}
	i += len(sub)
	return s[:i], s[i:], true
}

// Cut slices s around the first two appearances of sep. The found result reports whether sep appears at least twice in s. If sep does not appear in s, cut returns "", "", false.
//
//	s0, s1, s2, _ := fmt.Println(Cut2("a__b__c__d", "__")
//	fmt.Printf("%s %s %s\n", s0, s1, s2)
//	>>  a b c__d
func Cut2(text, sub string) (s0, s1, s2 string, ok bool) {
	s0, rem, ok := Cut(text, sub)
	if !ok {
		return "", "", "", false
	}
	if s1, s2, ok = Cut(rem, sub); !ok {
		return "", "", "", false
	}
	return s0, s1, s2, ok
}

// CutByte is as cut, but sep is a byte, not string.
func CutByte(s string, sep byte) (string, string, bool) {
	i := strings.IndexByte(s, sep)
	if i == -1 {
		return "", "", false
	}
	return s[:i], s[i+1:], true
}

// CutByteOnce is as CutAfter, but delim is a byte, not a string.
func CutByteAfter(s string, delim byte) (string, string, bool) {
	i := strings.IndexByte(s, delim)
	if i == -1 {
		return "", "", false
	}
	return s[:i+1], s[i+1:], true
}

// Cut2Byte is as Cut2, but sep is a byte, not string
func Cut2Byte(text string, sep byte) (s0, s1, s2 string, ok bool) {
	s0, rem, ok := CutByte(text, sep)
	if !ok {
		return "", "", "", false
	}
	s1, s2, ok = CutByte(rem, sep)
	return s0, s1, s2, ok
}

// CutRune is as Cut, but separates on a rune, not a string.
func CutRune(s string, r rune) (string, string, bool) {
	i := strings.IndexRune(s, r)
	if i == -1 {
		return "", "", false
	}
	return s[:i], s[i+utf8.RuneLen(r):], true
}

func CutRuneAfter(s string, r rune) (string, string, bool) {
	i := strings.IndexRune(s, r)
	if i == -1 {
		return "", "", false
	}
	i += utf8.RuneLen(r)
	return s[:i], s[i:], true
}

// Cut2Rune is as Cut2, but sep is a rune, not a string.
func Cut2Rune(text string, sep rune) (s0, s1, s2 string, ok bool) {
	s0, rem, ok := CutRune(text, sep)
	if !ok {
		return "", "", "", false
	}
	if s1, s2, ok = CutRune(rem, sep); !ok {
		return "", "", "", false
	}
	return s0, s1, s2, ok
}
