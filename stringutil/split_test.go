package stringutil_test

import (
	"testing"

	. "gitlab.com/efronlicht/estd/stringutil"
)

// runTest on a testCase by comparing the result of f to wantA, wantB, and wantOK
type testCase[T any] struct {
	s            string
	t            T
	wantA, wantB string
	wantOK       bool
}

func runTest[T any](t *testing.T, f func(string, T) (string, string, bool), tc []testCase[T]) {
	for _, tt := range tc {
		a, b, ok := f(tt.s, tt.t)
		if a != tt.wantA || b != tt.wantB || ok != tt.wantOK {
			t.Errorf("expected f(%v, %v) == %q, %q, %v, but got %q, %q, %v", tt.s, tt.t, tt.wantA, tt.wantB, tt.wantOK, a, b, ok)
		}
	}
}

func TestSplitOnce(t *testing.T) {
	t.Run("runes", func(t *testing.T) {
		t.Run("before", func(t *testing.T) {
			runTest(t, CutRune, []testCase[rune]{
				{s: "abc🔥🔥def", t: '🔥', wantA: "abc", wantB: "🔥def", wantOK: true},
				{s: "abcdef", t: '🔥'},
				{s: "🔥🔥🔥", t: '🔥', wantA: "", wantB: "🔥🔥", wantOK: true},
				{s: "abc!def", t: '!', wantA: "abc", wantB: "def", wantOK: true},
				{s: "abcdef", t: '!'},
				{s: "!!!", t: '!', wantA: "", wantB: "!!", wantOK: true},
			})
		})
		t.Run("after", func(t *testing.T) {
			runTest(t, CutRuneAfter, []testCase[rune]{
				{s: "abc🔥🔥def", t: '🔥', wantA: "abc🔥", wantB: "🔥def", wantOK: true},
				{s: "abcdef", t: '🔥'},
				{s: "🔥🔥🔥", t: '🔥', wantA: "🔥", wantB: "🔥🔥", wantOK: true},
				{s: "abc!!def", t: '!', wantA: "abc!", wantB: "!def", wantOK: true},
				{s: "abcdef", t: '!'},
				{s: "!!!", t: '!', wantA: "!", wantB: "!!", wantOK: true},
			})
		})
	})
	t.Run("bytes", func(t *testing.T) {
		t.Run("before", func(t *testing.T) {
			runTest(t, CutByte, []testCase[byte]{
				{s: "abc!def", t: '!', wantA: "abc", wantB: "def", wantOK: true},
				{s: "abcdef", t: '!'},
				{s: "!!!", t: '!', wantA: "", wantB: "!!", wantOK: true},
			})
		})
		t.Run("after", func(t *testing.T) {
			runTest(t, CutByteAfter, []testCase[byte]{
				{s: "abc!!def", t: '!', wantA: "abc!", wantB: "!def", wantOK: true},
				{s: "abcdef", t: '!'},
				{s: "!!!", t: '!', wantA: "!", wantB: "!!", wantOK: true},
			})
		})
	})
	t.Run("substr", func(t *testing.T) {
		t.Run("once", func(t *testing.T) {
			runTest(t, Cut, []testCase[string]{
				{s: "abc!🔥!🔥def", t: "!🔥", wantA: "abc", wantB: "!🔥def", wantOK: true},
				{s: "abcdef", t: "!🔥", wantA: "abcdef"},
				{s: "!🔥!🔥!🔥", t: "!🔥", wantA: "", wantB: "!🔥!🔥", wantOK: true},
			})
		})
		t.Run("after", func(t *testing.T) {
			runTest(t, CutAfter, []testCase[string]{
				{s: "abc!🔥!🔥def", t: "!🔥", wantA: "abc!🔥", wantB: "!🔥def", wantOK: true},
				{s: "abcdef", t: "!🔥", wantA: "abcdef"},
				{s: "!🔥!🔥!🔥", t: "!🔥", wantA: "!🔥", wantB: "!🔥!🔥", wantOK: true},
			})
		})
	})
}

func testSplit[T any](f func(s string, t T) (string, string, bool)) {
}
