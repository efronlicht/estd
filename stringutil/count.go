package stringutil

// CountBytes returns the number of copies of b in s.
//
//	CountBytes("aabcc", 'b')
//	>> 2
func CountBytes(s string, b byte) (count int) {
	for i := range s {
		if s[i] == b {
			count++
		}
	}
	return count
}

// CountRunes returns the number of copies of r in s.
// Be warned that unicode-equivalent code points do not always compare equally.
//
//	CountRunes(`🔥aaab🔥c🔥`, '🔥')
//	>> 2
func CountRunes(s string, r rune) (count int) {
	for _, q := range s {
		if q == r {
			count++
		}
	}
	return count
}

// RunesIn is the total number of runes in a string.
//
//	fmt.Println(RunesIn(`🔥💧`))
//	>> 2
func RunesIn(s string) (runes int) {
	for range s {
		runes++
	}
	return runes
}
