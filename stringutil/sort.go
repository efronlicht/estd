package stringutil

import (
	"sort"

	"golang.org/x/exp/constraints"
)

func sorted[T constraints.Ordered](a []T) []T {
	sort.Slice(a, func(i, j int) bool { return a[i] < a[j] })
	return a
}

// SortedBytes returns the bytes of the string, sorted as u8s.
func SortedBytes(s string) []byte { return sorted([]byte(s)) }

// SortedRunes returns the runes of the string, sorted as i32s.
// be warned that unicode-equivalent strings do not necessarially sort identically, due to composed & decomposed forms, unicode equivalence, etc.
func SortedRunes(s string) []rune { return sorted([]rune(s)) }
