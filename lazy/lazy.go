// Package lazy allows for lazily-initialized variables via New().
// These are often used for globals representing dependencies.
// The following example shows how to use enve and lazy to configure and
// initialize a postgres database using github.com/lib/pq.
//
//		import (
//	    	"gitlab.com/efronlicht/estd/enve"
//	    	"gitlab.com/efronlicht/estd/lazy"
//	    	"database/sql"
//	 	_ "github.com/lib/pq" // register postgres sql driver
//		)
//			var Postgres = lazy.New[*sql.DB](lazy.Unwrap(func() *sql.DB, error) {
//				return sql.Open("postgres", pgConnStrFromEnv())
//			})
//			func pgConnStrFromEnv() string
//				const (
//					defaultUser = "efron"
//					defaultDBName = "examples"
//					defaultSSLMode = "require"
//				)
//				return fmt.Sprintf("user=%s dbname=%s sslmode=%s",
//					enve.StringOr("PG_USER", defaultUser),
//					enve.StringOr("PG_DBNAME", defaultDB),
//					enve.StringOr("sslmode", defaultSSL),
//				)
//			}
package lazy

import (
	"fmt"
	"sync"
)

// Lazy[T] evaluates a value of type T exactly once, by calling New().
type lazy[T any] struct {
	once sync.Once
	new  func() T
	t    T
}

// New creates a lazy-evaluating value of type T. The first (and ONLY the first) call to Get() on the resulting value
// will call f() and cache and return it's value. Subsequent calls will return the same value.
func New[T any](f func() T) *lazy[T] {
	if f == nil {
		panic(fmt.Errorf("expected a non-nil %T", f))
	}
	return &lazy[T]{new: f}
}

// Unwrap turns a func()(T, error) into a func() T, panicking if the error wasn't nil.
// Useful for lazily-evaluted dependencies.
func Unwrap[T any](f func() (T, error)) func() T {
	return func() T {
		t, err := f()
		if err != nil {
			panic(fmt.Errorf("failed to lazily initialize a %T: %w", t, err))
		}
		return t
	}
}

// Get the value. This evalutes exactly once.
func (lr *lazy[T]) Get() T {
	lr.once.Do(func() {
		lr.t = lr.new()
	})
	return lr.t
}
