package lazy_test

import (
	"errors"
	"log"
	"sync"
	"sync/atomic"
	"testing"

	"gitlab.com/efronlicht/estd/lazy"
)

var (
	j int64
	v = lazy.New(lazy.Unwrap(func() (int64, error) {
		return atomic.AddInt64(&j, 1), nil
	}))
)

func TestLazy_Edges(t *testing.T) {
	t.Run("unwrap", func(t *testing.T) {
		defer func() {
			if p := recover(); p == nil {
				t.Fatal("expected a panic")
			}
		}()
		lazy.New(lazy.Unwrap(func() (n struct{}, err error) { return n, errors.New("some error") })).Get()
	})
	t.Run("nil", func(t *testing.T) {
		defer func() {
			if p := recover(); p == nil {
				t.Fatal("expected a panic")
			}
		}()
		lazy.New[int](nil).Get()
	})
}

func TestLazy(t *testing.T) {
	wg := new(sync.WaitGroup)
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func() {
			defer wg.Done()
			n := v.Get()
			if atomic.LoadInt64(&j) != n {
				log.Fatalf("expected n == j, but got %d, %d", n, j)
			}
		}()
	}
	wg.Wait()
}
