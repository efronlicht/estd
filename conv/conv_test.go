package conv_test

import (
	"testing"

	"gitlab.com/efronlicht/estd/conv"
	"gitlab.com/efronlicht/estd/equal"
	"gitlab.com/efronlicht/estd/parse"
)

func TestConstrainWidenMap(t *testing.T) {
	if _, err := conv.ConstrainMap[string, string](map[string]any{
		"foo": "bar",
		"baz": 2,
	}); err == nil {
		t.Fatal("expected an error, but got none")
	}
	want := map[string]string{"foo": "bar", "bar": "baz"}
	got, _ := conv.ConstrainMap[string, string](conv.WidenMap(want))
	if !equal.Map(want, got) {
		t.Fatal("failed to convert")
	}
}

func TestSliceResult(t *testing.T) {
	got, err := conv.SliceResult([]string{"3.14", "500"}, parse.Float64)
	if err != nil {
		t.Fatal(err)
	}
	assertEqualSlice(t, []float64{3.14, 500}, got)
	if _, err := conv.SliceResult([]string{"200", "not a float"}, parse.Float64); err == nil {
		t.Fatal("expected an error ,but got nil")
	}
}

func TestMapResult(t *testing.T) {
	m := map[string]string{
		"decimal": "10",
		"hex":     "0xffff_ffff",
		"octal":   "0o77",
	}
	want := map[string]int{
		"decimal": 10,
		"hex":     0xffff_ffff,
		"octal":   0o77,
	}
	if got, err := conv.MapResult(m, parse.Int); err != nil || !equal.Map(got, want) {
		t.Fatalf("expected %v, but got %v", got, want)
	}
	if _, err := conv.MapResult(m, parse.Int16); err == nil {
		t.Fatalf("expected an error (out of range), but got nil")
	}
}

func TestFilterMap(t *testing.T) {
}

func TestKeys(t *testing.T) {
	got := conv.SortedKeys(map[string]bool{"b": true, "a": true, "c": true})
	want := []string{"a", "b", "c"}
	assertEqualSlice(t, want, got)
}

func TestSortedMapItems(t *testing.T) {
	wantKeys := []rune{'a', 'b', 'c'}
	wantVals := []int{1, 2, 3}

	gotKeys, gotVals := conv.SortedMapItems(map[rune]int{'c': 3, 'b': 2, 'a': 1})
	assertEqualSlice(t, wantKeys, gotKeys)
	assertEqualSlice(t, wantVals, gotVals)
}

func TestFromKeyVals(t *testing.T) {
	assertEqualMap(t, map[string]float64{"pi": 3.14, "two": 2}, conv.FromKeyVals([]conv.KeyVal[string, float64]{{"pi", 3.14}, {"two", 2}}))
}

func assertEqualMap[K, V comparable](t *testing.T, want, got map[K]V) {
	if !equal.Map(want, got) {
		t.Fatalf("expected %v, but got %v", want, got)
	}
}

func assertEqualSlice[T comparable](t *testing.T, want, got []T) {
	if !equal.Slice(want, got) {
		t.Fatalf("expected %v, but got %v", want, got)
	}
}

func TestMapAsKeyVals(t *testing.T) {
}

func TestSlice(t *testing.T) {
	assertEqualSlice(t, []string{"a", "b", "c"}, conv.Slice([]rune{'a', 'b', 'c'}, func(r rune) string { return string([]rune{r}) }))
}

func TestMapFromItems(t *testing.T) {
	got := conv.MustMapFromItems([]string{"a", "b", "c"}, []int{1, 2, 3})
	want := map[string]int{"a": 1, "b": 2, "c": 3}
	assertEqualMap(t, want, got)
	if _, err := conv.MapFromItems([]string{"a", "b"}, []int(nil)); err == nil {
		t.Fatal("expected an error: different len")
	}
}
