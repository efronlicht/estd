package conv

import (
	"fmt"
	"sort"

	"gitlab.com/efronlicht/estd/internal/dynamic"
	"golang.org/x/exp/constraints"

	"gitlab.com/efronlicht/estd/must"
)

// ConstrainMap attempts to convert a map[K]any to a map with more concrete values.
// It returns an error if it an encounters an element not of type T.
func ConstrainMap[K comparable, V any](src map[K]any) (map[K]V, error) {
	dst := make(map[K]V, len(src))
	for k, sv := range src {
		dv, ok := sv.(V)
		if !ok {
			return nil, fmt.Errorf("conv.ConstrainMap[%T, %T]: key %v: expected a %T, but got %+v", src, dst, k, dv, sv)
		}
		dst[k] = dv
	}
	return dst, nil
}

// WidenMap converts a map[K][V] to a map[K] any.
func WidenMap[K comparable, V any](src map[K]V) map[K]any {
	dst := make(map[K]any, len(src))
	for k, v := range src {
		dst[k] = v
	}
	return dst
}

// FilterMap maps a map[KS]VS to a map[KS]VS by applying f and dropping the elements where f(ks, vs) returns _, _, false.
// eg:
//
//	toPositiveInt := func(s string) (int, bool) {
//		n, err := strconv.Atoi(s)
//		if err != nil || n <= 0 {
//			return n, false
//		}
//		return n, true
//	}
//	FilterMap[map[string]{v:}]
func FilterMap[KS, KD comparable, VS, VD any](
	src map[KS]VS,
	f func(KS, VS) (k KD, v VD, ok bool),
) map[KD]VD {
	dst := make(map[KD]VD)
	for k0, v0 := range src {
		k1, v1, ok := f(k0, v0)
		if !ok {
			continue
		}
		dst[k1] = v1
	}
	return dst
}

// MapResult attempts to convert a map[K0][V0] to a map[K1][V1], returning early if it encounters an error.
func MapResult[K comparable, V0, V1 any](src map[K]V0, f func(V0) (V1, error)) (map[K]V1, error) {
	dst := make(map[K]V1, len(src))
	for k, vs := range src {
		vd, err := f(vs)
		if err != nil {
			return nil, fmt.Errorf("MapResult %T to a %T: could not map key-value pair %v: %v: %w", src, dst, k, vs, err)
		}
		dst[k] = vd
	}
	return dst, nil
}

// Keys returns a slice of the map's keys in an arbitrary order.
func Keys[K comparable, V any](m map[K]V) []K {
	keys := make([]K, len(m))
	var i int
	for k := range m {
		keys[i] = k
		i++
	}
	return keys
}

// Keys returns a slice of the map's keys in a sorted order.
func SortedKeys[K constraints.Ordered, V any](m map[K]V) []K {
	keys := Keys(m)
	sort.Slice(keys, func(i, j int) bool { return keys[i] < keys[j] })
	return keys
}

// MapVals returns a slice of the map's values in an arbitrary order.
func MapVals[K comparable, V any](m map[K]V) []V {
	vals := make([]V, len(m))
	var i int
	for _, v := range m {
		vals[i] = v
		i++
	}
	return vals
}

// KeyVal represents a key:value pair. Useful as a compact and stack-alloc-able representation of a map entry.
type KeyVal[K, V any] struct {
	K K
	V V
}

// FromKeyVals creates a map from the slice a by setting m[kv.K] = kv.V for each item in a.
// This expects unique, non-NaN keys. The behavior of duplicate keys or NaN is not specified.
func FromKeyVals[K comparable, V any](a []KeyVal[K, V]) map[K]V {
	m := make(map[K]V, len(a))
	for _, kv := range a {
		m[kv.K] = kv.V
	}
	return m
}

// AsKeyVals creates a slice of []KeyVal{K,V} in an arbitrary order.
// See MapItems if you'd rather have a pair of slices.
func MapAsKeyVals[K comparable, V any](m map[K]V) []KeyVal[K, V] {
	kv := make([]KeyVal[K, V], len(m))
	var i int
	for k, v := range m {
		kv[i] = KeyVal[K, V]{K: k, V: v}
		i++
	}
	return kv
}

// SortedMapKeyVals creates a slice of []KeyVal{K,V} sorted by the keys.
func SortedMapKeyVals[K constraints.Ordered, V any](m map[K]V) []KeyVal[K, V] {
	kvs := MapAsKeyVals(m)
	sort.Slice(kvs, func(i, j int) bool { return kvs[i].K < kvs[j].K })
	return kvs
}

// Items returns the keys and values of the map m.
// keys[i] always corresponds to values[i], but otherwise they're in arbitrary oder.
// Use MapAsKeyVals if you'd rather have a slice of structs.
func MapItems[K comparable, V any](m map[K]V) (keys []K, vals []V) {
	keys, vals = make([]K, len(m)), make([]V, len(m))
	var i int
	for k, v := range m {
		keys[i], vals[i] = k, v
		i++
	}
	return keys, vals
}

// SortedMapItems turns a map[K][V] into a pair of slices where
// keys[i] always corresponds to values[i], sorted by the keys.
// That is, if i < j, keys[i] <= keys[j].
func SortedMapItems[K constraints.Ordered, V any](m map[K]V) (keys []K, vals []V) {
	k, v := MapItems(m)
	dynamic.Sort(
		len(k),
		func(i, j int) { k[i], k[j], v[i], v[j] = k[j], k[i], v[j], v[i] }, // swap
		func(i, j int) bool { return k[i] < k[j] },                         // less
	)
	return k, v
}

// SliceResult attempts to convert a slice of []T to a slice of []B, returning when it encounters it's first error.
// This is equivalent to the "map" function in Python, Rust, etc.
// In general, it's better to just use a for loop for clarity: Go doesn't really support the functional style.
func SliceResult[T, B any](in []T, f func(T) (B, error)) (out []B, err error) {
	out = make([]B, len(in))
	for i := range in {
		if out[i], err = f(in[i]); err != nil {
			// conv.SliceResult: 'string' -> 'int': failed on item #3: unexpected character 🔥'
			return out, fmt.Errorf("conv.SliceResult: '%T'  '%T']: failed on item %d: %w", in[i], out[i], i, err)
		}
	}
	return out, nil
}

// SliceResult attempts to convert a slice of []T to a slice of []B.
// This is roughly equivalent to the "map" function in Python, Rust, etc.
// In general, it's better to just use a for loop for clarity: Go doesn't really support the functional style.
func Slice[T, B any](in []T, f func(T) B) []B {
	out := make([]B, len(in))
	for i := range in {
		out[i] = f(in[i])
	}
	return out
}

// MustMapFromItems creates a map from equal-length slices k and v.
// Constraints:
//   - The slices must be of equal length
//   - the items in k must be distinct.
//
// Violation of either panics.
func MustMapFromItems[K comparable, V any](k []K, v []V) map[K]V {
	return must.Unwrap(MapFromItems(k, v))
}

// FromItems creates a map from equal-length slices k and v.
// Constraints:
//   - The slices must be of equal length
//   - the items in k must be distinct.
//
// Violation of either returns an error.
func MapFromItems[K comparable, V any](k []K, v []V) (map[K]V, error) {
	if len(k) != len(v) {
		return nil, fmt.Errorf("expected equal length keys and vals, but len(k)=%d, len(v)=%d", len(k), len(v))
	}
	m := make(map[K]V, len(k))
	for i := range k {
		if _, ok := m[k[i]]; ok {
			return nil, fmt.Errorf("expected distinct keys, but had duplicate key %v", k[i])
		}
		m[k[i]] = v[i]
	}
	return m, nil
}
