package ectx

import (
	"context"
	"testing"
	"time"
)

func TestStripped(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)
	defer cancel()
	time.Sleep(30 * time.Millisecond)
	if ctx.Err() == nil {
		t.Fail()
	}
	if StripCancel(ctx).Err() != nil {
		t.Fail()
	}
}

func TestFromContext(t *testing.T) {
	_, ok := Val[int](context.Background())
	if ok {
		t.Fail()
	}
	_, ok = Val[int](WithVal(context.Background(), 2))
	if !ok {
		t.Fail()
	}
}

func TestValue(t *testing.T) {
	ctx := context.Background()
	if got := ValOrZero[int](ctx); got != 0 {
		t.Fatal("expected 0")
	}

	if got := ValOr(WithVal(ctx, 2), 0); got != 2 {
		t.Fatal("expected 2")
	}
	// TypeKey of type aliases should be different
	type string2 string

	ctx = WithVal(ctx, "bar")
	if got := ValOrElse(ctx, func() string2 { return "foo" }); got != "foo" {
		t.Fatal("expected foo")
	}
}
