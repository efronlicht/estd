// Go programmers often use contexts to store request-scoped information, especially metadata or log fields.
// In general, these are singletons per type: it doesn't make sense for a context to have multiple IDs, origins, or loggers.
// The functions in this package provide a simple and useful API for working with contexts in this way.
//
// # QUICK START
//
// Save and retrieve values from a context:
//
//	ctx := ectx.WithValue(context.Background(), zap.L().With(zap.String("author", "efron")))
//
// Get a value from the context:
//
//	log, ok := ectx.Val[*zap.Logger](ctx)
//
// Get a value from a context, using a default if it's missing
//
//	log := ectx.Val(ctx, log.Default())
//
// Get a value from a context, or call the provided function if it's missing
//
//	log := ectx.ValOr(ctx, log.Default) // note the missing ()
//
// # Word of warning
//
// The context is often misused as a general-purpose key-value storage, which causes a ton of harm.
// This ectx's api _deliberately omits_ the more general context.WithValue to discourage this.
// Ask yourself two questions before putting a value in the context rather than passing it directly as a value in the function signature.
//
// 1: Is this PART of the function, or is it ABOUT the function?
//
// 2: Should the function proceed without the value (i.e, through a sensible default or no-op?)
//
// Only if both are true should you use ectx. When in doubt, error on the side of injecting dependencies directly.
package ectx

import (
	"context"
	"fmt"
	"time"
)

// TypeKey is a zero-sized type that acts as a context key for the type T.
// You can't store multiple values of the same type with a TypeKey!
// If you're worried about that, don't use the functions in this package!
type TypeKey[T any] struct{}

// Val looks up the value stored under TypeKey[T]{}.
func Val[T any](ctx context.Context) (T, bool) {
	t, ok := ctx.Value(TypeKey[T]{}).(T)
	return t, ok
}

// ValOrZero is as FromCtx, but ignores the 'ok'.
func ValOrZero[T any](ctx context.Context) T { t, _ := ctx.Value(TypeKey[T]{}).(T); return t }

// ValOr gets the entry stored under TypeKey[T], if it exists, returning  fallback otherwise.
func ValOr[T any](ctx context.Context, fallback T) T {
	t, ok := ctx.Value(TypeKey[T]{}).(T)
	if !ok {
		return fallback
	}
	return t
}

// MustVal gets the entry stored under TypeKey[T], panicking otherwise.
//
// # Warning
//
// Be careful when and where you use this function! Anything you MUST have should probably be in the function signature, not the context.
// This can be useful for tests & in rare other occasions and is included for completion.
func MustVal[T any](ctx context.Context) T {
	t, ok := ctx.Value(TypeKey[T]{}).(T)
	if !ok {
		panic(fmt.Errorf("expected a value of type %T stored in the context under %T, but there was none", t, TypeKey[T]{}))
	}
	return t
}

// WithVal returns a new context with the value under a TypeKey[T]
func WithVal[T any](ctx context.Context, t T) context.Context {
	return context.WithValue(ctx, TypeKey[T]{}, t)
}

// ValOrElse is like ValOr, but lazily evaluated.
func ValOrElse[T any](ctx context.Context, f func() T) T {
	t, ok := ctx.Value(TypeKey[T]{}).(T)
	if !ok {
		return f()
	}
	return t
}

// stripped is a context stripped of it's cancellations or deadlines, but retaining it's other values.
type stripped struct{ context.Context }

// Err is alwayhs nil.
func (s stripped) Err() error { return nil }

// Done is a nil channel.
func (s stripped) Done() <-chan struct{} { return nil }

// Deadline always returns time.Time{}, false.
func (s stripped) Deadline() (t time.Time, ok bool) { return t, false }

// StripCancel returns a context which inherits it's parents values, but NOT it's deadline or cancellation.
func StripCancel(ctx context.Context) context.Context { return stripped{ctx} }
