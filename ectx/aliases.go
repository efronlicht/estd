package ectx

import (
	"context"
	"time"
)

//	// alias for context.TODO()
//
// TODO returns a non-nil, empty Context. Code should use context.TODO when it's unclear which Context to use or it is not yet available (because the surrounding function has not yet been extended to accept a Context parameter).
func TODO() context.Context { return context.TODO() }

//	// alias for context.Background
//
// Background returns a non-nil, empty Context. It is never canceled, has no values, and has no deadline. It is typically used by the main function, initialization, and tests, and as the top-level Context for incoming requests.
func Background() context.Context { return context.Background() }

//	// alias for context.WithDeadline
//
// WithDeadline returns a copy of the parent context with the deadline adjusted to be no later than d. If the parent's deadline is already earlier than d, WithDeadline(parent, d) is semantically equivalent to parent. The returned context's Done channel is closed when the deadline expires, when the returned cancel function is called, or when the parent context's Done channel is closed, whichever happens first.
// Canceling this context releases resources associated with it, so code should call cancel as soon as the operations running in this Context complete.
func WithDeadline(parent context.Context, deadline time.Time) (ctx context.Context, cancel context.CancelFunc) {
	return context.WithDeadline(parent, deadline)
}

// // alias for context.WithCancel
// WithCancel returns a copy of parent with a new Done channel. The returned context's Done channel is closed when the returned cancel function is called or when the parent context's Done channel is closed, whichever happens first.
// Canceling this context releases resources associated with it, so code should call cancel as soon as the operations running in this Context complete.
func WithCancel(parent context.Context) (ctx context.Context, cancel context.CancelFunc) {
	return context.WithCancel(parent)
}

//	// alias for context.WithTimeout
//
// WithTimeout returns WithDeadline(parent, time.Now().Add(timeout)).
// Canceling this context releases resources associated with it, so code should call cancel as soon as the operations running in this Context complete:
func WithTimeout(parent context.Context, timeout time.Duration) (ctx context.Context, cancel context.CancelFunc) {
	return context.WithTimeout(parent, timeout)
}
