package dynamic

import "sort"

type sorter struct {
	// len is a stub for sort.Interface.Len
	len int
	// swap is a stub for sort.Interface.Swap
	swap func(i, j int)
	// less is a stub for sort.Interface.Less
	less func(i, j int) bool
}

// Sort anything using the provided swap and comparisons.
func Sort(len int, swap func(i, j int), less func(i, j int) bool) {
	sort.Sort(sorter{len: len, swap: swap, less: less})
}

var _ sort.Interface = sorter{}

func (s sorter) Len() int           { return s.len }
func (s sorter) Swap(i, j int)      { s.swap(i, j) }
func (s sorter) Less(i, j int) bool { return s.less(i, j) }
