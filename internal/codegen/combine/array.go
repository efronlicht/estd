package main

import (
	"fmt"
	"io"
)

const preLoopFmt = `
// Combine%02d feeds the input from %02d inputs into a single output.
// It does NOT close the output channel.
func _Combine%02d[T any](ctx context.Context, dst chan <- T, src  *[%d]<- chan T,) (int, error) {
	const n = %d
	done := 0
  	for {
		if done == n {
			return n, ctx.Err()
		}
		select {
		case <- ctx.Done():
			return done, ctx.Err()
`

const inLoop = `
		case t, ok := <- src[%d]:
			if !ok {
				src[%d] = nil // select from this can't succeed
				done++
			} else {
				dst <- t
			}`

func writeArray(w io.Writer, n int) error {
	if n < 0 {
		return fmt.Errorf("expected a positive integer, but got %d", n)
	}
	if _, err := fmt.Fprintf(w, preLoopFmt, n, n, n, n, n); err != nil {
		return err
	}
	for i := 0; i < n; i++ {
		if _, err := fmt.Fprintf(w, inLoop, i, i); err != nil {
			return err
		}
	}
	_, err := w.Write([]byte("\n\t\t}\n\t}\n}"))
	return err
}
