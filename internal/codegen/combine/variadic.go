package main

import (
	"fmt"
	"io"
)

func writeVariadic(w io.Writer, n int) {
	fmt.Fprintf(w, `
// CombineN feeds up to 10 input channels into a single output channel.
// Bounds: N <= %02d
func CombineN[T any](ctx context.Context, dst chan <- T, src ... <- chan T) (int, error) {
	switch len(src) {
	default:
		panic("no implementation for n > %02d")
	`, n, n)
	for i := 0; i <= n; i++ {
		const format = `
		case %d:
			return _Combine%02d(ctx, dst, (*[%d]<- chan T)(src),)`
		fmt.Fprintf(w, format, i, i, i)
	}
	fmt.Fprintf(w, `
	}
	// unreachable!
}`)
}
