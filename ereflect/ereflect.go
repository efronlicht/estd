package ereflect

import "reflect"

// TypeOf returns the reflect.Type of T
func TypeOf[T any]() reflect.Type { return reflect.TypeOf((*T)(nil)).Elem() } // never constructs a T, no matter how large.
