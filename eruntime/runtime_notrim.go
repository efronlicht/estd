//go:build eruntime_notrim

package eruntime

// TrimFunction is a stub that does nothing, because the eruntime_notrim build tag is enabled.
func TrimFunction(s string) string {
	if _, after, ok := strings.Cut(s, "efronlicht"); ok && strings.HasPrefix(after, "estd") {
		return after
	}
	return s
}
