//go:build !estd_nozerolog && !estd_nodeps

package eruntime

import (
	"github.com/rs/zerolog"
)

var (
	_ zerolog.LogObjectMarshaler = (*FuncInfo)(nil)
	_ zerolog.LogArrayMarshaler  = (StackSlice)(nil)
)

func (fi *FuncInfo) MarshalZerologObject(e *zerolog.Event) {
	e.Str("function", fi.Function).Str("file", fi.File).Str("label", fi.Label).Int("line", fi.Line)
}

func (s StackSlice) MarshalZerologArray(a *zerolog.Array) {
	for i := range s {
		a = a.Object(s[i])
	}
}
