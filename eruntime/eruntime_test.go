package eruntime_test

import (
	"strings"
	"testing"

	. "gitlab.com/efronlicht/estd/eruntime"
)

func Test_ThisFile(t *testing.T) {
	if !strings.Contains(ThisFile(), "eruntime/eruntime_test") {
		t.Fatalf(`!strings.Contains(eruntime.ThisFile(), "eruntime/eruntime_test"`)
	}
}

func Test_ThisDir(t *testing.T) {
	if !strings.Contains(ThisDir(), "eruntime") {
		t.Fatalf(`!strings.Contains(eruntime.ThisDir()`)
	}
	if strings.Contains(ThisDir(), "eruntime/eruntime_test") {
		t.Fatalf(`strings.Contains(eruntime.ThisDir(), "eruntime/eruntime_test")`)
	}
}

func Test_Caller(t *testing.T) {
	c := a()
	if !strings.Contains(c.Function, "a") {
		t.Fatal("expected 'a', got " + c.Function)
	}
	const want = "some_label"
	if c.Label != want {
		t.Fatalf("expected '%s', but got '%s'", want, c.Label)
	}
}

func b() *FuncInfo { return LabeledCaller("some_label", 1) }

func a() *FuncInfo { return b() }
