//go:build !estd_nozap && !estd_no_deps

package eruntime

import "go.uber.org/zap/zapcore"

var (
	_ zapcore.ObjectMarshaler = (*FuncInfo)(nil)
	_ zapcore.ArrayMarshaler  = StackSlice(nil)
)

func (fi *FuncInfo) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	oe.AddString("file", fi.File)
	oe.AddString("function", fi.Function)
	oe.AddInt("line", fi.Line)
	oe.AddString("label", fi.Label)
	return nil
}

func (s StackSlice) MarshalLogArray(ae zapcore.ArrayEncoder) error {
	for i := range s {
		_ = ae.AppendObject(s[i])
	}
	return nil
}
