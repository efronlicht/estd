package eruntime

import (
	"fmt"
	"math"
	"path"
	"path/filepath"
	"runtime"
	"strings"
)

type (
	// FuncInfo is limited function metadata, often embedded in logs.
	// It has a handy String() which looks like this:
	// Get a FuncInfo with NamedFunc() or Caller().
	FuncInfo struct {
		// Function name, obtained via runtime.FuncForPC.Name().
		Function string
		// Filename, including path.
		File  string
		Label string // optional label
		Line  int
	}
	// StackSlice is a []*FuncInfo that nicely formats for fmt.Stringer, zapcore.ArrayMarshaler, and zerolog.ZerologArrayMarshaler.
	StackSlice []*FuncInfo
)

// Is64Bit is true on 64-bit systems. (In 2022, that's nearly everything).
const Is64Bit = math.MaxInt64 == math.MaxInt

func ThisFileLine() (file string, line int) { _, file, line, _ = runtime.Caller(1); return file, line }

// ThisFile returns the file of it's caller.
func ThisFile() string { _, f, _, _ := runtime.Caller(1); return f }

// ThisDir returns the directory of it's caller.
func ThisDir() string { _, f, _, _ := runtime.Caller(1); return filepath.Dir(f) }

var _, _ fmt.Stringer = (*FuncInfo)(nil), StackSlice(nil)

func LabeledCaller(name string, skip int) *FuncInfo {
	c := Caller(skip + 1)
	c.Label = name
	return c
}

// Callers obtains a miniature stack trace of at most n elements starting at skip via runtime.Callers(skip+1).
// It returns at most n elements, but may return fewer, or even zero (if skip is larger than the stack).
func Callers(skip, n int) (callers StackSlice) {
	pc := make([]uintptr, n)
	n = runtime.Callers(skip+1, pc)
	if n == 0 {
		return nil
	}

	pc = pc[:n] // pass only valid pcs to runtime.CallersFrames
	frames := runtime.CallersFrames(pc)
	callers = make(StackSlice, 0, n)
	// Loop to get frames.
	// A fixed number of PCs can expand to an indefinite number of Frames.
	for {
		f, more := frames.Next()
		callers = append(callers, &FuncInfo{Function: TrimFunction(f.Function), File: f.File, Line: f.Line})
		if !more {
			return callers
		}
	}
	// unreachable!
}

func (c *FuncInfo) Base() string {
	return path.Base(c.Function)
}

func (c *FuncInfo) Package() string {
	return path.Dir(c.Function)
}

// Caller reports the file, line number, and name of the function skip levels up the stack.
// skip=0 is the caller of Caller:
//
//	func a() { return fmt.Println(Caller(1))}
//	func b() { a() } // will print info about b(), not a().
func Caller(skip int) *FuncInfo {
	pc, file, line, _ := runtime.Caller(skip + 1)
	f := runtime.FuncForPC(pc)

	return &FuncInfo{Function: TrimFunction(f.Name()), File: file, Line: line}
}

func (c *FuncInfo) String() string {
	if c.Label != "" {
		return fmt.Sprintf("%s:%s:L%04d (%s)", c.Function, c.File, c.Line, c.Label)
	}
	return fmt.Sprintf("%s:%s:L%04d", c.Function, c.File, c.Line)
}

func (s StackSlice) String() string {
	b := new(strings.Builder)
	b.WriteString("<stack[")
	for i := range s {
		fmt.Fprintf(b, "\n%02d:\t%s", i, s[i])
	}
	b.WriteString("\n]>")
	return b.String()
}
