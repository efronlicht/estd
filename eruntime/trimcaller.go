//go:build !eruntime_notrim

package eruntime

import "strings"

// TrimFunction trims a function path containing "efronlicht/estd", making it start with "estd".
// EG, "users/efron/go/src/gitlab.com/efronlicht/estd/parse/IPv4" => "estd/parse.IPv4".
// this looks nice, but has a runtime cost.
// use the build tag "eruntime_notrim" to disable it
func TrimFunction(s string) string {
	if _, after, ok := strings.Cut(s, "efronlicht/"); ok && strings.HasPrefix(after, "estd") {
		return after
	}
	return s
}
