package internal

import "time"

type Cached[V any] struct {
	Val     V
	Expires time.Time
}
