package cache_test

import (
	"context"
	"encoding/binary"
	"fmt"
	"os"
	"testing"
	"time"

	"gitlab.com/efronlicht/estd/cache"
)

type BigEndian uint64

func (be BigEndian) String() string { return fmt.Sprint(uint64(be)) }
func (be *BigEndian) MarshalBinary() ([]byte, error) {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(*be))
	return b, nil
}

func (be *BigEndian) UnmarshalBinary(b []byte) error {
	*be = BigEndian(binary.BigEndian.Uint64(b))
	return nil
}

const (
	invalidation = 50 * time.Millisecond
	cleanup      = 50 * time.Millisecond
)

func TestDiscCache(t *testing.T) {
	t.Parallel()

	dir := os.TempDir()
	const invalidation = 100 * time.Millisecond
	called := 0
	f := func(ctx context.Context, be BigEndian) (BigEndian, error) {
		called++
		return be, nil
	}
	c, err := cache.NewDisc(f, dir, invalidation)
	if err != nil {
		t.Log(err)
	}
	t.Log("checking cache")
	_, found, err := c.Check(context.TODO(), 3)
	if err != nil || found {
		t.Fatalf("%v: no keys should be set yet", err)
	}
	got, err := c.Get(context.TODO(), 4)
	if err != nil {
		t.Fatal(err)
	}
	if got != 4 || called != 1 {
		t.Fatalf("expected callcount == 1, got == 4, but got %d, %d", called, got)
	}

	_, found, err = c.Check(context.TODO(), 4)
	if err != nil || !found {
		t.Fatalf("keys should be set now!")
	}
	// subsequent calls should be cached!
	got, _ = c.Get(context.TODO(), 4)
	if got != 4 || called != 1 {
		t.Fatalf("expected callcount == 1, got == 4, but got %d, %d", called, got)
	}

	time.Sleep(5 * invalidation)
	// cache should be expired.
	got, _ = c.Get(context.TODO(), 4)
	if called != 2 || got != 4 {
		t.Fatalf("expected callcount == 9, got == 3, but got %d, %d", called, got)
	}
}

func TestInMemCache(t *testing.T) {
	t.Parallel()
	called := 0
	f := func(ctx context.Context, n int) (int, error) {
		called++
		time.Sleep(invalidation / 10)
		return n, nil
	}
	t.Logf("checking cache is empty")
	c := cache.NewInMem(f, invalidation, cleanup)
	_, found, err := c.Check(context.TODO(), 3)
	if err != nil || found {
		t.Fatalf("no keys should be set yet")
	}
	t.Logf("cache is empty")
	got, _ := c.Get(context.TODO(), 4)
	if got != 4 || called != 1 {
		t.Fatalf("expected callcount == 1, got == 4, but got %d, %d", called, got)
	}

	_, found, err = c.Check(context.TODO(), 4)
	if err != nil || !found {
		t.Fatalf("keys should be set now!")
	}
	// subsequent calls should be cached!
	if got, _ = c.Get(context.TODO(), 4); got != 4 || called != 1 {
		t.Fatalf("expected callcount == 1, got == 4, but got %d, %d", called, got)
	}

	time.Sleep(5 * invalidation)
	// cache should be expired. calls to Check shouldn't refresh the cache.
	if _, found, _ = c.Check(context.TODO(), 4); called != 1 || found {
		t.Fatalf("cache should be expired. calls to Check shouldn't refresh the cache")
	}
	got, _ = c.Get(context.TODO(), 4)
	if called != 2 || got != 4 {
		t.Fatalf("expected callcount == 9, got == 3, but got %d, %d", called, got)
	}
}
