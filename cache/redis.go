//go:build !estd_nodeps

package cache

import (
	"context"
	"encoding"
	"errors"
	"fmt"
	"time"

	redis "github.com/go-redis/redis/v9"
	"gitlab.com/efronlicht/estd/parse"
)

var _ Interface[String, Int] = (*Redis[String, Int, *Int])(nil)

// Redis is an expiring cache for a function where values are stored in a redis instance.
// This is safe for concurrent use among multiple programs & goroutines.
type Redis[K fmt.Stringer, V any, PV interface {
	encoding.TextMarshaler
	encoding.TextUnmarshaler
	*V
}] struct {
	redis        *redis.Client
	expiresAfter time.Duration
	f            func(context.Context, K) (V, error)
}

// NewRedis builds a cache that
func NewRedis[K fmt.Stringer, V any, PV interface {
	encoding.TextMarshaler
	encoding.TextUnmarshaler
	*V
}](
	redis *redis.Client,
	expiresAfter time.Duration,
	f func(context.Context, K) (V, error),
) Redis[K, V, PV] {
	switch {
	case redis == nil:
		panic("nil redis")
	case expiresAfter < 0:
		panic(fmt.Errorf("expected a non-negative expiration, but had %s", expiresAfter))
	case f == nil:
		panic("can't cache a nil function")
	default:
		return Redis[K, V, PV]{redis: redis, expiresAfter: expiresAfter, f: f}
	}
}

var _ Interface[Int, Int] = (*Redis[Int, Int, *Int])(nil)

func (r *Redis[K, V, PV]) Invalidate(ctx context.Context, k K) error {
	err := r.redis.Del(ctx, k.String()).Err()
	if err == nil || errors.Is(err, redis.Nil) {
		return nil
	}
	return err
}

func (r *Redis[K, V, PV]) Get(ctx context.Context, k K) (v V, err error) {
	s, err := r.redis.Get(ctx, k.String()).Result()
	if err == nil {
		return parse.FromText[V, PV](s)
	}
	if errors.Is(err, redis.Nil) {
		return r.Refresh(ctx, k)
	}
	return v, err
}

func (r *Redis[K, V, PV]) Refresh(ctx context.Context, k K) (v V, err error) {
	v, err = r.f(ctx, k)
	if err != nil {
		return v, err
	}
	b, err := (PV)(&v).MarshalText()
	if err != nil {
		return v, fmt.Errorf("called the underlying %T without error, but failed to seralize a %T using MarshalText(): %w", r.f, PV(nil), err)
	}
	r.redis.Set(ctx, k.String(), string(b), r.expiresAfter)
	return v, nil
}

func (r *Redis[K, V, PV]) Check(ctx context.Context, k K) (v V, found bool, err error) {
	s, err := r.redis.Get(ctx, k.String()).Result()
	if errors.Is(err, redis.Nil) {
		return v, false, nil
	}
	if err != nil {
		return v, false, err
	}
	v, err = parse.FromText[V, PV](s)
	return v, true, err
}

var _ Interface[String, Int] = (*Redis[String, Int, *Int])(nil)
