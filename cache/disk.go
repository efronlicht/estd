package cache

import (
	"bytes"
	"context"
	"encoding"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/efronlicht/estd/reexport/lockedfile"
)

// Disc is a cache which uses the host filesystem. It is safe for concurrent use WITHIN the same program, but not ACROSS programs.
// there's still some bugs to work out re: locking the host filesystem, especially across platforms.
// use at your own risk.
type Disc[K fmt.Stringer, V any, PV interface {
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler
	*V
}] struct {
	dir        string
	f          func(ctx context.Context, k K) (V, error)
	expiration time.Duration
	cancel     chan struct{}
}

func NewDisc[K fmt.Stringer, V any, PV interface {
	encoding.BinaryMarshaler
	encoding.BinaryUnmarshaler
	*V
}](
	f func(context.Context, K) (V, error),
	dir string,
	expiration time.Duration,
) (*Disc[K, V, PV], error) {
	abs, err := filepath.Abs(dir)
	if err != nil {
		return nil, fmt.Errorf("could not canonicalize path %q: %w", dir, err)
	}

	if err := os.MkdirAll(abs, 0o664); err != nil {
		return nil, fmt.Errorf("failed to find or create directory at %q: %w", abs, err)
	}

	if expiration < 0 {
		return nil, fmt.Errorf("expected a non-negative expiration, but got %s", expiration)
	}
	return &Disc[K, V, PV]{
		dir:        abs,
		f:          f,
		expiration: expiration,
		cancel:     make(chan struct{}),
	}, errors.New("WARNING: disc cache has some issues right now")
}

func (d Disc[K, V, PV]) path(k K) string { return d.dir + "/" + k.String() }

func (d Disc[K, V, PV]) Invalidate(ctx context.Context, k K) error { return os.Remove(d.path(k)) }
func (d *Disc[K, V, PV]) Refresh(ctx context.Context, k K) (v V, err error) {
	v, err = d.f(ctx, k)
	if err != nil {
		return v, fmt.Errorf("error from underlying function: %w", err)
	}

	b, err := PV(&v).MarshalBinary()
	if err != nil {
		return v, fmt.Errorf("calling underlying function was OK, but couldn't marshal the result to binary: %w", err)
	}
	if err := lockedfile.Write(d.path(k), bytes.NewReader(b), 0o644); err != nil {
		return v, fmt.Errorf("failed to write to file at %v: %w", d.path(k), err)
	}
	return v, nil
}

func (d *Disc[K, V, PV]) Check(ctx context.Context, k K) (v V, fresh bool, err error) {
	path := d.path(k)
	f, err := lockedfile.Open(path)

	// TODO: figure out behavior when someone else has the lock.
	// what is the error returned? does it vary by OS? what approach should we take? e,g,
	// retry until the timeout?
	if errors.Is(err, os.ErrNotExist) {
		return v, false, nil
	}
	if err != nil {
		return v, false, fmt.Errorf("failed to read cache:  opening file: %w", err)
	}
	defer func() {
		if closeErr := f.Close(); closeErr != nil {
			err = fmt.Errorf("%w: additional error closing file: %v", err, closeErr)
		}
	}()
	{
		var fi fs.FileInfo
		fi, err = f.Stat()
		if err != nil {
			return v, false, fmt.Errorf("failed to read cache: failed to stat file: %w", err)
		}

		if expires := fi.ModTime().Add(d.expiration); expires.Before(time.Now()) {
			return v, false, nil
		}
	}
	{

		var b []byte
		b, err = io.ReadAll(f)
		if err != nil {
			return v, false, err
		}
		if err = PV(&v).UnmarshalBinary(b); err != nil {
			err = fmt.Errorf("failed to read cache: failed to unmarshal %d bytes as a %T:  %w", len(b), PV(nil), err)
		}
		return v, true, nil

	}
}

func (d *Disc[K, V, PV]) Get(ctx context.Context, k K) (v V, err error) {
	v, fresh, err := d.Check(ctx, k)
	if err != nil {
		return v, err
	}
	if fresh {
		return v, nil
	}
	return d.Refresh(ctx, k)
}
