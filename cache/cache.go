// TODO: readme, etc
package cache

import "context"

// Interface represents a cache of an underlying function. It's satisfied by InMem, Disk, and Redis.
type Interface[K, V any] interface {
	// Invalidate the cached result for K.
	Invalidate(context.Context, K) error
	// Check the cache for a result without refreshing it.
	Check(context.Context, K) (v V, fresh bool, err error)
	// Get the cached result for K, refreshing the cache if necessary.
	Get(context.Context, K) (V, error)
	// Refresh gets a fresh result and refreshes the cache.
	Refresh(context.Context, K) (V, error)
}

var (
	_ Interface[string, string] = (*InMem[string, string])(nil)
	_ Interface[String, Int]    = (*Disc[String, Int, *Int])(nil)
)
