package cache

import (
	"encoding/binary"
	"fmt"
	"strconv"
	"unsafe"

	"golang.org/x/exp/constraints"
)

// type aliases for stdlib prmitives to help with disk & redis.

// String is an alias for the primitive string that implements fmt.Stringer.
type String string

// Int8..=Int64 are aliases for the primitives that implement fmt.Stringer, encoding.TextMarshaler, and encoding.Unmarshaler.
type (
	Int   int
	Int8  int8
	Int16 int16
	Int32 int32
	Int64 int64
)

// Unt8..=UInt64 are aliases for the primitives that implement fmt.Stringer, encoding.TextMarshaler, and encoding.Unmarshaler.
type (
	Uint   uint
	Uint8  uint8
	Uint16 uint16
	Uint32 uint32
	Uint64 uint64
)

// Float32 and Float64 are aliases for the primitives that implement fmt.Stringer, encoding.TextMarshaler (on *T), and encoding.Unmarshaler (on *T).
type (
	Float32 float32
	Float64 float64
)

// String returns the underlyign string.
func (s String) String() string { return string(s) }

// String formats w/ strconv.FormatInt(int64(n), 10)
func (n Int8) String() string { return strconv.FormatInt(int64(n), 10) }

// String formats w/ strconv.FormatInt(int64(n), 10)
func (n Int16) String() string { return strconv.FormatInt(int64(n), 10) }

// String formats w/ strconv.FormatIint(int64(n), 10)
func (n Int32) String() string { return strconv.FormatInt(int64(n), 10) }

// String formats w/ strconv.FormatInt(int64(n), 10)
func (n Int64) String() string { return strconv.FormatInt(int64(n), 10) }

// String formats w/ strconv.FormatInt(int64(n), 10)
func (n Int) String() string { return strconv.FormatInt(int64(n), 10) }

// String formats w/ strconv.FormatUint(uint64(n), 10)
func (u Uint8) String() string { return strconv.FormatUint(uint64(u), 10) }

// String formats w/ strconv.FormatUint(uint64(n), 10)
func (u Uint16) String() string { return strconv.FormatUint(uint64(u), 10) }

// String formats w/ strconv.FormatUint(uint64(n), 10)
func (u Uint32) String() string { return strconv.FormatUint(uint64(u), 10) }

// String formats w/ strconv.FormatUint(uint64(n), 10)
func (u Uint64) String() string { return strconv.FormatUint(uint64(u), 10) }

// String formats w/ strconv.FormatInt(int64(n), 10)
func (u Uint) String() string { return strconv.FormatUint(uint64(u), 10) }

// UnmarshalText via strconv.ParseUint.
func (u *Uint8) UnmarshalText(b []byte) error { return _uint_unmarshalText(b, u, 8) }

// UnmarshalText via strconv.ParseUint.
func (u *Uint16) UnmarshalText(b []byte) error { return _uint_unmarshalText(b, u, 16) }

// UnmarshalText via strconv.ParseUint.
func (u *Uint32) UnmarshalText(b []byte) error { return _uint_unmarshalText(b, u, 32) }

// UnmarshalText via strconv.ParseUint.
func (u *Uint64) UnmarshalText(b []byte) error { return _uint_unmarshalText(b, u, 64) }

// UnmarshalText via strconv.ParseUint.
func (u *Int) UnmarshalText(b []byte) error { return _int_unmarshalText(b, u, 0) }

// UnmarshalText via strconv.ParseUint.
func (u *Int8) UnmarshalText(b []byte) error  { return _int_unmarshalText(b, u, 8) }
func (u *Int16) UnmarshalText(b []byte) error { return _int_unmarshalText(b, u, 16) }

// UnmarshalText via strconv.ParseInt.
func (u *Int32) UnmarshalText(b []byte) (err error) { return _int_unmarshalText(b, u, 32) }

// UnmarshalText via strconv.ParseInt.
func (u *Int64) UnmarshalText(b []byte) error { return _int_unmarshalText(b, u, 64) }

func _uint_unmarshalText[T constraints.Unsigned](b []byte, pt *T, bitsize int) error {
	n, err := strconv.ParseUint(string(b), 0, int(unsafe.Sizeof(*pt)))
	*pt = T(n)
	return err
}

func _int_unmarshalText[T constraints.Signed](b []byte, pt *T, bitsize int) error {
	n, err := strconv.ParseInt(string(b), 0, int(unsafe.Sizeof(*pt)))
	*pt = T(n)
	return err
}

// MarshalText calls String().
func _stringer_marshalText[V fmt.Stringer](pv *V) ([]byte, error) {
	return []byte(((*pv).String())), nil
}

func _int_marshalBinaryLE[T constraints.Integer](t T) ([]byte, error) {
	var buf [8]byte
	binary.LittleEndian.PutUint64(buf[:], uint64(t))
	return buf[:], nil
}

func _int_unmarshalBinaryLE[T constraints.Integer](b []byte, pt *T) error {
	*pt = T(binary.LittleEndian.Uint64(b))
	return nil
}

// MarshalText calls String().
func (u *Int) MarshalText() ([]byte, error) { return _stringer_marshalText(u) }

// MarshalText calls String().
func (u *Int8) MarshalText() ([]byte, error) { return _stringer_marshalText(u) }

// MarshalText calls String().
func (u *Int16) MarshalText() ([]byte, error) { return _stringer_marshalText(u) }

// MarshalText calls String().
func (u *Int32) MarshalText() ([]byte, error) { return _stringer_marshalText(u) }

// MarshalText calls String().
func (u *Int64) MarshalText() ([]byte, error) { return _stringer_marshalText(u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Int8) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Int16) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Int32) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Int64) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Int) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Uint8) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Uint16) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Uint32) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Uint64) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// MarshalBinary marshals the integer type as 8 bytes, little-endian, regardless of source size.
func (u *Uint) MarshalBinary() ([]byte, error) { return _int_marshalBinaryLE(*u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Int8) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Int16) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Int32) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Int64) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Int) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Uint8) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Uint16) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Uint32) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Uint64) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }

// Unmarshal.Binary unmarshals the integer from the first eight bytes, little-endian.
// THERE IS NO ERROR CHECKING WHATSOEVER.
func (u *Uint) UnmarshalBinary(b []byte) error { return _int_unmarshalBinaryLE(b, u) }
