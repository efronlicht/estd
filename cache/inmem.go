package cache

import (
	"context"
	"fmt"
	"runtime"
	"time"

	"gitlab.com/efronlicht/estd/cache/internal"
	"gitlab.com/efronlicht/estd/containers/esync"
)

// InMem is an expiring cache for a function where values are stored in memory.
// This is safe for concurrent use among any number of goroutines.
type InMem[K, V any] struct {
	cancel          chan struct{}
	m               esync.Map[K, *internal.Cached[V]]
	invalidateAfter time.Duration
	f               func(ctx context.Context, k K) (V, error)
}

// Cancel the cache, freeing up it's resources. It is undefined behavior to use the cache after cancellation.
func (im *InMem[K, V]) Cancel() { close(im.cancel) }

// NewInMem creates an expiring cache for a function where values are stored in memory.
func NewInMem[K, V any](
	f func(context.Context, K) (V, error),
	invalidateAfter, cleanupEvery time.Duration,
) *InMem[K, V] {
	switch { // check bounds
	case f == nil:
		panic(fmt.Errorf("expected a non-nil %T", f))
	case invalidateAfter <= 0, cleanupEvery <= 0:
		panic(fmt.Errorf("expected %T invalidateAfter=%q and cleanupEvery=%q to be positive", invalidateAfter, invalidateAfter, cleanupEvery))
	case cleanupEvery < invalidateAfter:
		panic(fmt.Errorf("expected %T cleanupEvery (%q) <= invalidateAfter (%q)", cleanupEvery, cleanupEvery, invalidateAfter))
	}
	im := &InMem[K, V]{f: f, invalidateAfter: invalidateAfter, cancel: make(chan struct{})}
	go im.startCleanup(cleanupEvery)
	runtime.SetFinalizer(im, (*InMem[K, V]).Purge)
	return im
}

// Purge the cache as completely as possible.
// Because this uses sync.Map.Range, this does not represent any consistent snapshot of the underlying map.
func (im *InMem[K, V]) Purge() {
	im.m.Range(func(k K, _ *internal.Cached[V]) bool {
		im.m.Delete(k)
		return true
	})
}

// remove expired entries every so often.
func (im *InMem[K, V]) startCleanup(every time.Duration) {
	t := time.NewTicker(every)
	defer t.Stop()
	for {
		select {
		case now := <-t.C:
			im.m.Range(func(k K, c *internal.Cached[V]) bool {
				if c.Expires.Before(now) {
					im.m.Delete(k)
				}
				return true
			})
		case <-im.cancel:
			return
		}
	}
}

func (im *InMem[K, V]) Expiration() time.Duration { return im.invalidateAfter }

// Refresh the value for key K by calling the underyling function and returning it.
func (im *InMem[K, V]) Refresh(ctx context.Context, k K) (V, error) {
	v, err := im.f(ctx, k)
	if err != nil {
		return v, err
	}
	im.m.Store(k, &internal.Cached[V]{Val: v, Expires: time.Now().Add(im.invalidateAfter)})
	return v, err
}

// Invalidate the cached result for k. Never returns an error.
func (im *InMem[K, V]) Invalidate(_ context.Context, k K) error { im.m.Delete(k); return nil }

// Check the cache for the item without refreshing it.
func (im *InMem[K, V]) Check(ctx context.Context, k K) (v V, fresh bool, err error) {
	if err := ctx.Err(); err != nil {
		return v, false, err
	}
	c, ok := im.m.Load(k)
	if !ok {
		return v, false, err
	}
	if time.Now().After(c.Expires) {
		return v, false, err
	}
	return c.Val, true, err
}

// Get the item by loading a fresh cache value, Refresh()-ing the cache if necessary.
func (im *InMem[K, V]) Get(ctx context.Context, k K) (v V, err error) {
	if err := ctx.Err(); err != nil {
		return v, err
	}
	c, ok := im.m.Load(k)
	if !ok {
		return im.Refresh(ctx, k)
	}
	return c.Val, err
}
