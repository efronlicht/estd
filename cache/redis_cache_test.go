//go:build !estd_nodeps

package cache_test

import (
	"context"
	"strconv"
	"testing"
	"time"

	miniredis "github.com/alicebob/miniredis/v2"
	redis "github.com/go-redis/redis/v9"
	. "gitlab.com/efronlicht/estd/cache"
)

func TestRedisCache(t *testing.T) {
	s := miniredis.RunT(t)
	t.Parallel()
	var calls int
	toInt := func(_ context.Context, k String) (Int, error) {
		calls++
		n, err := strconv.Atoi(string(k))
		return Int(n), err
	}

	const expiration = time.Second
	c := NewRedis(redis.NewClient(&redis.Options{Addr: s.Addr()}), expiration, toInt)
	got, err := c.Get(context.TODO(), "20")
	if err != nil {
		t.Fatal(err)
	}
	if got != 20 {
		t.Fatal(got)
	}
	if calls != 1 {
		t.Fatalf("expected 1 call, got %d", calls)
	}
	// should be cached
	c.Get(context.TODO(), "20")
	if calls != 1 {
		t.Fatalf("expected 1 call, got %d", calls)
	}
	if _, found, _ := c.Check(context.TODO(), "20"); !found {
		t.Fatal("expected found")
	}
	s.FastForward(2 * time.Second)
	// cache should have naturally expired
	if _, found, _ := c.Check(context.TODO(), "20"); found {
		t.Fatal("expected not found")
	}
	if calls != 1 {
		t.Fatalf("expected 1 call, got %d", calls)
	}
	c.Get(context.TODO(), "20")
	if calls != 2 {
		t.Fatalf("expected 2 calls, got %d", calls)
	}
	// manually invalidate
	c.Invalidate(context.TODO(), "20")
	c.Get(context.TODO(), "20")
	if calls != 3 {
		t.Fatalf("expected 3 calls, got %d", calls)
	}
}
