//go:build !estd_nozaplog && !estd_nodeps

package zaputil_test

import (
	"encoding/json"
	"reflect"
	"testing"

	"gitlab.com/efronlicht/estd/zaputil"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest"
)

type NilObjectMarshaler struct{}

func (NilObjectMarshaler) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	return nil
}

func Test_Adapters(t *testing.T) {
	t.Parallel()
	buf := new(zaptest.Buffer)
	o := zaputil.OMap[zapcore.ObjectMarshaler]{
		"ints":  zaputil.IMap[int]{"1": 1, "-1": -1},
		"uints": zaputil.UMap[uint]{"0": 0},
		"arrays": zaputil.AMap[zapcore.ArrayMarshaler]{
			"uints": zaputil.UArray[uint]{0, 1, 2},
			"ints":  zaputil.IArray[int]{-1, -2, 3},
			"aa":    zaputil.AArray[zapcore.ArrayMarshaler]{zaputil.UArray[uint]{0, 1, 2}, zaputil.IArray[int]{-1, -2, 3}},
			"oa":    zaputil.OArray[NilObjectMarshaler]{{}, {}, {}, {}, {}},
		},
	}
	log := zap.New(zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewDevelopmentEncoderConfig()), buf, zapcore.DebugLevel))
	log.Info("testing adapters", zap.Object("object", o))
	log.Sync()
	var got struct {
		L, T, M string
		Object  struct {
			Uints  map[string]uint64
			Ints   map[string]int64
			Arrays struct {
				Uints [3]uint64
				Ints  [3]int64
				AA    [2][3]int64
				OA    [5]NilObjectMarshaler
			}
		}
	}

	t.Log(buf.String())
	d := json.NewDecoder(buf)
	d.DisallowUnknownFields()
	if err := d.Decode(&got); err != nil {
		t.Fatal(err)
	}
	mustEq := func(want, got any) {
		if !reflect.DeepEqual(want, got) {
			t.Fatalf("expected %+v == %+v", want, got)
		}
	}
	mustEq(got.Object.Ints, map[string]int64{"1": 1, "-1": -1})
	mustEq(got.Object.Uints, map[string]uint64{"0": 0})
	mustEq(got.Object.Arrays, struct {
		Uints [3]uint64
		Ints  [3]int64
		AA    [2][3]int64
		OA    [5]NilObjectMarshaler
	}{
		Uints: [3]uint64{0, 1, 2},
		Ints:  [3]int64{-1, -2, 3},
		AA:    [2][3]int64{{0, 1, 2}, {-1, -2, 3}},
		OA:    [5]NilObjectMarshaler{},
	})
}
