//go:build !estd_nozaplog && !estd_nodeps

package zaputil

import (
	"fmt"

	"go.uber.org/zap/zapcore"
	"golang.org/x/exp/constraints"
)

// object encoders
type (
	// OMap provides a generic implementation of zapcore.ObjectMarshaler for a map from strings to elements which are themselves ObjectMarshalers.
	OMap[O zapcore.ObjectMarshaler] map[string]O
	// IMap provides a generic implementation of zapcore.ObjectMarshaler for maps from strings to signed ints.
	IMap[N constraints.Signed] map[string]N
	// UMap provides a generic implementation of zapcore.ObjectMarshaler for maps from stringst o unsigned ints.
	UMap[U constraints.Unsigned] map[string]U
	// AMap provides a generic implementation of zapcore.ArrayMarshaler for slices whose elements are ObjectMarshalers.
	AMap[A zapcore.ArrayMarshaler] map[string]A
)

// array encoders
type (

	// A provides a generic implementation of zapcore.ArrayMarshaler for slices whose elements are themselves ArrayMarshalers.
	AArray[A zapcore.ArrayMarshaler] []A
	IArray[N constraints.Signed]     []N
	UArray[U constraints.Unsigned]   []U
	// OArray provides a generic implementation of zapcore.ObjectMarshaler for slices whose elements are ObjectMarshalers.
	OArray[O zapcore.ObjectMarshaler] []O
)

var (
	_ zapcore.ObjectMarshaler = OMap[UMap[uint64]](nil)
	_ zapcore.ArrayMarshaler  = OArray[OMap[IMap[int64]]](nil)
)

func (om OMap[T]) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	for k, v := range om {
		if err := oe.AddObject(k, v); err != nil {
			return fmt.Errorf("%T.MarshalLogObject: adding object with key %s: %v", om, k, err)
		}
	}
	return nil
}

func (am AMap[A]) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	for k, v := range am {
		if err := oe.AddArray(k, v); err != nil {
			return fmt.Errorf("%T.MarshalLogObject: adding array of key %v: %v", am, k, err)
		}
	}
	return nil
}

func (im IMap[T]) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	for k, v := range im {
		oe.AddInt64(k, int64(v))
	}
	return nil
}

func (um UMap[T]) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	for k, v := range um {
		oe.AddUint64(k, uint64(v))
	}
	return nil
}

func (aa AArray[A]) MarshalLogArray(ae zapcore.ArrayEncoder) error {
	for i := range aa {
		err := ae.AppendArray(aa[i])
		if err != nil {
			return fmt.Errorf("%T.MarshalLogArray: appending array #%d: %v", aa, i, err)
		}

	}
	return nil
}

func (oa OArray[T]) MarshalLogArray(ae zapcore.ArrayEncoder) error {
	for i := range oa {
		if err := ae.AppendObject(oa[i]); err != nil {
			return fmt.Errorf("%T.MarshalLogArray: appending object #%d: %v", oa, i, err)
		}
	}
	return nil
}

func (ua UArray[U]) MarshalLogArray(ae zapcore.ArrayEncoder) error {
	for _, n := range ua {
		ae.AppendUint64(uint64(n))
	}
	return nil
}

func (in IArray[N]) MarshalLogArray(ae zapcore.ArrayEncoder) error {
	for _, n := range in {
		ae.AppendInt64(int64(n))
	}
	return nil
}
