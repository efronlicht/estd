package ops_test

import (
	"testing"

	. "gitlab.com/efronlicht/estd/ops"
)

func testBinop[T any, V comparable](t *testing.T, op string, f func(T, T) V, a, b T, want V) {
	t.Run(op, func(t *testing.T) {
		t.Parallel()
		if got := f(a, b); got != want {
			t.Errorf("expected %v, but got %v", want, got)
		}
	})
}

func Test_BinaryOps(t *testing.T) {
	t.Parallel()
	// arithmetic
	testBinop(t, "+", Add[int], 2, 3, 2+3)
	testBinop(t, "-", Sub[int], 5, 3, 5-3)
	testBinop(t, "*", Mul[int], 2, 3, 2*3)
	testBinop(t, "/", Div[int], 6, 3, 6/3)
	testBinop(t, "%", Mod[int], 6, 5, 6%5)
	// comparisons
	testBinop(t, "!=", Neq[int], 0, 1, 0 != 12)
	testBinop(t, "<", Less[int], 0, 1, 0 < 1)
	testBinop(t, "<=", Leq[int], -1, -1, -1 <= -1)
	testBinop(t, "==", Eq[int], 3, 2, 3 == 2)
	testBinop(t, "==", Eq[int], 3, 3, 3 == 3)
	testBinop(t, ">", Greater[int], 1, 0, 1 > 0)
	testBinop(t, ">=", Geq[int], 2, 3, 2 >= 3)
	testBinop(t, ">=", Geq[int], 3, 3, 3 >= 3)
	// bitwise
	testBinop(t, "&", BitAnd[uint], 0b101, 0b011, 0b101&0b011)
	testBinop(t, "^", BitXor[uint], 0b101, 0b110, 0b101^0b110)
	testBinop(t, "<<", ShiftLeft[uint64], 0b01, 0b10, 0b01<<0b10)
	testBinop(t, ">>", ShiftRight[uint64], 0b1011, 0b10, 0b1011>>0b10)
	testBinop(t, "|", BitOr[uint], 0b101, 0b110, 0b101|0b110)
}

func testUnop[T comparable](t *testing.T, op string, f func(T) T, input, want T) {
	t.Run(op, func(t *testing.T) {
		t.Parallel()
		if got := f(input); got != want {
			t.Errorf("expected %v, but got %v", want, got)
		}
	})
}

func Test_UnaryOps(t *testing.T) {
	t.Parallel()
	testUnop(t, "-", Negate[int], 2, -(2))
	testUnop(t, "^", BitInvert[uint8], 0b1111_0000, ^uint8(0b1111_0000))
	testUnop(t, "id", Identity[int], 2, +(2))
}
