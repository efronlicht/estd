// Package ops contains generics for go's built-in operators.
// two-operator arithmetic:
//
//	+: Add
//	%: Mod
//	*: Mul
//	/: Div
//
// two-operator comparison
//
//	==: Eq
//	 <: Less
//	<=: Leq
//	 >: Gt
//	>=: Geq
//
// two-operator binary arithmetic:
//
//	 |: BinOr
//	 &: BinAnd
//	 ^: BinXor
//	>>: ShiftRight
//	<<: ShiftLeft
package ops

import (
	"golang.org/x/exp/constraints"
)

// Number is uint8..=64, int8..=64, float32..=64, complex64..=128
type Number interface {
	constraints.Float | constraints.Integer | constraints.Complex
}

// Add is the + operator. It works on strings, too!
func Add[T Number | ~string](a, b T) T { return a + b }

// Mul is the * operator.
func Mul[T Number](a, b T) T { return a * b }

// Div is the * operator.
func Div[T Number](a, b T) T { return a / b }

// Sub is the - operator.
func Sub[T Number](a, b T) T { return a - b }

// Mod is the % operator.
func Mod[T constraints.Integer](a, b T) T { return a % b }

// Eq is the == operator.
func Eq[T comparable](a, b T) bool { return a == b }

// Neq is the != operator.
func Neq[T comparable](a, b T) bool { return a != b }

// Less is the < operator.
func Less[T constraints.Ordered](a, b T) bool { return a < b }

// Leq is the < operator.
func Leq[T constraints.Ordered](a, b T) bool { return a <= b }

// Geq is the >= operator.
func Geq[T constraints.Ordered](a, b T) bool { return a >= b }

// Greater is the > operator.
func Greater[T constraints.Ordered](a, b T) bool { return a > b }

// BitOr is the | operator.
func BitOr[T constraints.Integer](a, b T) T { return a | b }

// BitAnd is the & operator.
func BitAnd[T constraints.Integer](a, b T) T { return a & b }

// BitXor is the ^ operator.
func BitXor[T constraints.Integer](a, b T) T { return a ^ b }

// ShiftLeft is the << operator.
func ShiftLeft[T constraints.Integer](a, b T) T { return a << b }

// ShiftRight is the >> operator.
func ShiftRight[T constraints.Integer](a, b T) T { return a >> b }
