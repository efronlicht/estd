package ops

import "golang.org/x/exp/constraints"

// BinInvert is the bitwise negation operator, unary ^.
func BitInvert[T constraints.Integer](a T) T { return ^a }

// Negate returns the negative of the integer.
func Negate[T constraints.Signed | constraints.Float | constraints.Complex](a T) T { return -a }

// Identity returns T.
func Identity[T any](a T) T { return a }
