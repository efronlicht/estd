package set

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/efronlicht/estd/reexport/fmtsort"
)

// HSet is a non-concurrency-safe set backed by a map[T]bool.
// The set must be initialized before use, either via make(HSet) or New[Hset[K]]
type HSet[K comparable] map[K]struct{}

var unit struct{}

func (s HSet[K]) Len() int { return len(s) }

// String() returns "{k0, k1, k2}"... where ki is the ith key formatted as though with "%v".
// The keys are always returned in a consistent order. The sort rules are those used by the stdlib's internal/fmtsort:
//   - when applicable, nil compares low
//   - ints, floats, and strings order by <
//   - NaN compares less than non-NaN floats
//   - bool compares false before true
//   - complex compares real, then imag
//   - pointers compare by machine address
//   - channel values compare by machine address
//   - structs compare each field in turn
//   - arrays compare each element in turn; otherwise identical arrays compare by length.
//   - interface values compare first by reflect.Type describing the concrete type; and then by concrete value as described in the previous rules.
func (s HSet[K]) String() string {
	switch s.Len() {
	case 0:
		return "{}"
	case 1:
		for k := range s {
			return fmt.Sprintf("{%v}", k)
		}
		panic("unreachable!")
	default:
		var b strings.Builder
		keys := fmtsort.Sort(reflect.ValueOf(s)).Key
		fmt.Fprintf(&b, "{%v", keys[0])
		for _, k := range keys[1:] {
			fmt.Fprintf(&b, ", %v", k)
		}
		b.WriteByte('}')
		return b.String()
	}
}

func (s HSet[K]) Contains(k K) bool {
	if s.Len() == 0 {
		return false
	}

	_, ok := s[k]
	return ok
}

func (s HSet[K]) Remove(k K) {
	delete(s, k)
}

func (s HSet[K]) Insert(k K) { s[k] = unit }

// With Inserts a key K, returning the same set.
func (s HSet[K]) With(k K) HSet[K] { s[k] = unit; return s }

func (s HSet[K]) WithKeys(keys ...K) HSet[K] {
	for _, k := range keys {
		s[k] = unit
	}
	return s
}

// Clone returns a new set with a shallow copy of each of the keys.
func (s HSet[T]) Clone() HSet[T] {
	copied := make(map[T]struct{}, len(s))
	for k, v := range s {
		copied[k] = v
	}
	return copied
}

// Union of S and Q (S ∪ Q) returns a new set with the keys of both S and Q.
func (s HSet[T]) Union(q HSet[T]) (u HSet[T]) {
	u = s.Clone()
	for k := range q {
		u.Insert(k)
	}
	return u
}

// Difference(S, Q) 'S-Q' returns a new Set with the keys in S but not Q.
func (s HSet[T]) Difference(q HSet[T]) (d HSet[T]) {
	n := s.Len() - q.Len()
	if n < 0 {
		return make(HSet[T])
	}
	d = make(HSet[T], n)
	for k := range q {
		if !s.Contains(k) {
			d.Insert(k)
		}
	}
	return d
}

// Complement (S ∆ Q, sometimes called 'symmetric difference') returns a new set containing the keys that in S xor Q.
//
//	fmt.Println(Complement[int](New[int](1, 2), New[int](2,3)))
//	Output: true
func (s HSet[T]) Complement(q HSet[T]) (c HSet[T]) {
	c = make(HSet[T])
	for k := range s {
		if !q.Contains(k) {
			c.Insert(k)
		}
	}
	for k := range q {
		if !s.Contains(k) {
			c.Insert(k)
		}
	}
	return c
}

// Keys returns a slice of the keys of S. No order is guaranteed.
func (s HSet[K]) AsSlice() []K {
	a := make([]K, 0, s.Len())
	for k := range s {
		a = append(a, k)
	}
	return a
}

// Equal (S==Q) iff S and Q contain exactly the same keys.
func (s HSet[T]) Equal(q HSet[T]) bool {
	if q.Len() != s.Len() {
		return false
	}
	for _, k := range s.AsSlice() {
		if !q.Contains(k) {
			return false
		}
	}
	return true
}

// Intersection (A ∩ B) returns a new set containing the keys that are in both a and b.
func (s HSet[T]) Intersection(q HSet[T]) (is HSet[T]) {
	n := s.Len() - q.Len()
	if n < 0 {
		n = -n
		s, q = q, s
	}
	is = make(HSet[T], n)
	for _, k := range s.AsSlice() {
		if q.Contains(k) {
			is.Insert(k)
		}
	}
	return is
}
