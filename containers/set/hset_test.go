package set_test

import (
	"testing"

	. "gitlab.com/efronlicht/estd/containers/set"
)

func TestSet(t *testing.T) {
	a := make(HSet[string]).WithKeys("a", "c")
	b := make(HSet[string]).WithKeys("b", "c")
	if wantU, gotU := make(HSet[string]).WithKeys("a", "b", "c"), a.Union(b); !gotU.Equal(wantU) {
		t.Fatalf("expected %s, got %v", wantU, gotU)
	}
	if wantI, gotI := make(HSet[string]).WithKeys("c"), a.Intersection(b); !gotI.Equal(wantI) {
		t.Fatalf("expected %v, got %v", wantI, gotI)
	}
	want := make(HSet[string]).WithKeys("a", "b")
	if !a.Complement(b).Equal(want) {
		t.Fail()
	}

	if got := a.Difference(b).Union(b.Difference(a)); !got.Equal(want) {
		t.Fatalf("got %v, wanted %v", got, want)
	}
}
