// package containers/esync implements type-safe wrappers around the stdlib's sync.Pool and sync.Map,
// and a concurrency-safe set implementation (using sync.Map)
package esync

import (
	"sync"
)

// Map is a type-safe convenience wrapper around sync.Map. The documentation is taken from the documentation for sync.Map, (C) the go authors, as of go1.18, with minor edits to reflect the type changes.
//
// Map is like a Go map[K]V but is safe for concurrent use by multiple goroutines without additional locking or coordination. Loads, stores, and deletes run in amortized constant time.
// The Map type is optimized for two common use cases: (1) when the entry for a given key is only ever written once but read many times, as in caches that only grow, or (2) when multiple goroutines read, write, and overwrite entries for disjoint sets of keys. In these two cases, use of a Map may significantly reduce lock contention compared to a Go map paired with a separate Mutex or RWMutex.
// The zero Map is empty and ready for use. A Map must not be copied after first use.
type Map[K, V any] struct {
	inner sync.Map
}

// Range calls f sequentially for each key and value present in the map. If f returns false, range stops the iteration.
// Range does not necessarily correspond to any consistent snapshot of the Map's contents: no key will be visited more than once, but if the value for any key is stored or deleted concurrently (including by f), Range may reflect any mapping for that key from any point during the Range call. Range does not block other methods on the receiver; even f itself may call any method on m.
// Range may be O(N) with the number of elements in the map even if f returns false after a constant number of calls.
func (m *Map[K, V]) Range(f func(K, V) (next bool)) {
	m.inner.Range(func(k, v any) bool {
		return f(k.(K), v.(V))
	})
}

func (m *Map[K, V]) Store(k K, v V) { m.inner.Store(k, v) }

// LoadAndDelete deletes the value for a key, returning the previous value if any. The loaded result reports whether the key was present.
func (m *Map[K, V]) LoadAndDelete(k K) (v V, loaded bool) {
	raw, loaded := m.inner.LoadAndDelete(k)
	if !loaded {
		return v, false
	}
	return raw.(V), true
}

// Load returns the value stored in the map for a key. The ok result indicates whether value was found in the map.
func (m *Map[K, V]) Load(k K) (v V, ok bool) {
	raw, ok := m.inner.Load(k)
	if !ok {
		return v, ok
	}
	v, ok = raw.(V)
	return v, ok
}

// LoadOrStore returns the existing value for the key if present. Otherwise, it stores and returns the given value. The loaded result is true if the value was loaded, false if stored.
func (m *Map[K, V]) LoadOrStore(k K, v V) (actual V, loaded bool) {
	raw, loaded := m.inner.LoadOrStore(actual, loaded)

	return raw.(V), loaded
}

// Delete deletes the value for a key.
func (m *Map[K, V]) Delete(k K) { m.inner.Delete(k) }
