// Package deref contains utility functions for converting to and from pointer types and treating them as optional values.
// This is not always very useful and sometimes too clever. Use with caution.
package deref

// Or returns *pt if pt != nil, else fallback.
func Or[T any](pt *T, fallback T) T {
	if pt != nil {
		return *pt
	}
	return fallback
}

// ToTuple coerces a *pt to a (T, bool) tuple.
// If *pt != nil, returns t, true; else nil, false.
func ToTuple[T any](pt *T) (t T, ok bool) {
	if pt == nil {
		return t, false
	}
	return *pt, true
}

// PtrFromTuple turns a (T, bool) pair into a *T.
func PtrFromTuple[T any](t T, ok bool) *T {
	if ok {
		return &t
	}
	return nil
}

// OrElse is as Or, but lazily evaluates the fallback.
func OrElse[T any](pt *T, f func() T) T {
	if pt != nil {
		return *pt
	}
	return f()
}

// As tries to convert v to T, returning a (typed) nil if it's not the right type.
// Note that if the type of T is a non-nil *T, this will still return nil.
// EG, As[int](new(int)) == (*int)(nil).
func As[T any](v any) *T {
	t, ok := v.(T)
	if ok {
		return &t
	}
	return nil
}

// PtrToCopy returns a pointer to the value of t.
func PtrToCopy[T any](t T) *T { return &t }

// Map calls f(*pt) if pt != nil.
func Map[T any, B any](pt *T, f func(T) B) *B {
	if pt == nil {
		return nil
	}
	return PtrToCopy(f(*pt))
}
