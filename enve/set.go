package enve

import (
	"encoding"
	"fmt"
	"os"

	"golang.org/x/exp/constraints"
)

// SetInt sets the environment variable 'key' to the value of fmt.Sprint(t)
func SetInt[T constraints.Integer](key string, t T) error {
	return os.Setenv(key, fmt.Sprintf("%d", t))
}

func parseNoOp(key string) (string, error) { return key, nil }

// Set the environment variable key to the value of t.MarshalText().
func SetFromText(key string, t encoding.TextMarshaler) error {
	b, err := t.MarshalText()
	if err != nil {
		return fmt.Errorf("could not set environment variable %s: failed to marshal a value of type %T as text: %w", key, t, err)
	}
	if err := os.Setenv(key, string(b)); err != nil {
		return fmt.Errorf("could not set environment variable %s: %w", key, err)
	}
	return nil
}
