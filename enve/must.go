package enve

import (
	"time"

	"gitlab.com/efronlicht/estd/parse"
)

// MustParse looks up the envvar, panicking on a missing value.
func MustString(key string) string { return must(parseNoOp, key) }

// MustBool looks up the envvar, parsing as with parse.Bool, panicking on a missing value or error parsing.
func MustBool(key string) bool { return must(parse.Bool, key) }

// MustDuration looks up the envvar, parsing as with parse.Duration, panicking on a missing value or error parsing.
func MustDuration(key string) time.Duration { return must(parse.Duration, key) }

// MustInt is an alias for MustParse(parse.Int, key).
func MustInt16(key string) int16 { return must(parse.Int16, key) }
func MustInt32(key string) int32 { return must(parse.Int32, key) }
func MustInt64(key string) int64 { return must(parse.Int64, key) }
func MustInt8(key string) int8   { return must(parse.Int8, key) }
func MustInt(key string) int     { return must(parse.Int, key) }

// MustUint is an alias for MustParse(parse.Uint, key).
func MustUInt16(key string) uint16 { return must(parse.Uint16, key) }
func MustUInt32(key string) uint32 { return must(parse.Uint32, key) }
func MustUInt64(key string) uint64 { return must(parse.Uint64, key) }
func MustUInt8(key string) uint8   { return must(parse.Uint8, key) }
func MustUint(key string) uint     { return must(parse.Uint, key) }

// MustTimeRFC3339 looks up and parses the environment var key as a RFC3339 datetime, panicking on a missing value or failed parse.
func MustTimeRFC3339(key string) time.Time { return must(parse.TimeRFC3339, key) }

// MustParse looks up and parses a [T] using the provided function, panicking on a missing value or error parsing.
func MustParse[T any](parse func(string) (T, error), key string) T { return must(parse, key) }
