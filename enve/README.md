# enve

parsing and configuration via environment variables

## the big idea

If you have a **required** configuration value, use the **Must** family of functions:

```go
var publicKey = enve.MustGetString("MY_APP_PUBLIC_KEY")
```

If you have a **sensible default**, use the **Or** family of functions:

```go
// if MY_APP_PORT is set, use that, but otherwise, 8080
var port = enve.PortOr("MY_APP_PORT", 8080)
```

You can use custom parsing functions easily with MustParse or ParseOr:

```go

var requiredToken []byte = enve.MustParse(base64.StdEncoding.DecodeString, "REQUIRED_TOKEN_THATS_IN_BASE64_FOR_SOME_REASON")
type Point3 struct {X, Y, Z float64}
func pointFromString(s string)(p Point3, err error) {
    _, err := fmt.Sscan(s, &p.X, &p.Y, &p.Z)
    return p, err
}
var startingPoint = enve.ParseOr(pointFromString, "STARTING_POINT", Point3{0, 0, 0})
```
