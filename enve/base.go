// Package enve handles configuration through environment variables.
// Every primitive type is handled here, and many other types are available in the parse library.
//
// Usage:
//
//		// lookup CLIENT_TIMEOUT and parse it as a duration, returning 5*time.Second if it's missing or there's an error.
//	 var ClientTimeout = DurationOr("CLIENT_TIMEOUT", 5*time.Second)
//		// get the environment variable "GITHUB_AUTH_TOKEN", panicking if it's missing.
//	 var authToken = MustString("GITHUB_AUTH_TOKEN")
package enve

import (
	"os"

	"gitlab.com/efronlicht/estd/enve/log"
)

// MissingKeyError is returned from the Lookup series of functions (e.g, LookupParse) on a missing environment variable.
type MissingKeyError string

func (err MissingKeyError) Error() string { return "missing environment variable " + string(err) }

func lookup[T any](parse func(s string) (T, error), key string) (t T, err error) {
	s, ok := os.LookupEnv(key)
	if !ok {
		return t, MissingKeyError(key)
	}
	return parse(s)
}

// lookup(parse, key) and return the backup on an error.
func or[T any](parse func(string) (T, error), key string, backup T) T {
	t, err := lookup(parse, key)
	if err != nil {
		log.Or(key, err, parse, backup)
		return backup
	}
	return t
}

// lookup(parse, key) and panic on an error.
func must[T any](parse func(string) (T, error), key string) T {
	t, err := lookup(parse, key)
	if err != nil {
		log.Must(key, err, parse)
		panic(err) // reachable only if enve_logdisabled

	}
	return t
}

// LookupParse gets the env var specified by key and parses it as a T using parse.
// A missing value will return a MissingKeyError.
// A failure to parse will be a ParseError. It will Unwrap() to the error that parse returned.
func LookupParse[T any](parse func(string) (T, error), key string) (T, error) {
	t, err := lookup(parse, key)
	if err != nil {
		log.Lookup(key, err, parse)
		return t, err
	}
	return t, nil
}
