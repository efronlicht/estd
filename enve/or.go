package enve

import (
	"encoding"
	"errors"
	"strconv"
	"time"

	"gitlab.com/efronlicht/estd/parse"
	"golang.org/x/exp/constraints"
)

// BoolOr returns the boolean value represented by the environment variable.
// It accepts 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False. Any other value will return backup.
func BoolOr(key string, backup bool) bool { return or(strconv.ParseBool, key, backup) }

func notEmpty(s string) (string, error) {
	if s == "" {
		return "", errors.New("unexpected empty string")
	}
	return s, nil
}

// StringOr returns os.Getenv(key) if it's not the empty string, or backup otherwise.
func StringOr(key, backup string) string {
	return or(notEmpty, key, backup)
}

// ParseOr looks up the environment var key and parses the value using parse(). An error or missing value
// returns backup.
func ParseOr[T any](parse func(string) (T, error), key string, backup T) T {
	return or(parse, key, backup)
}

// GetFromText gets the encoding.TextUnmarshaler if present and unmarshable.
func FromTextOr[T any, PT interface {
	*T
	encoding.TextUnmarshaler
}](key string, backup T,
) T {
	return or(parse.FromText[T, PT], key, backup)
}

func PortOr[T constraints.Signed](key string, backup uint16) uint16 {
	return or(parse.Port, key, backup)
}

// MustTimeRFC3339 looks up and parses the environment var key as though using
func DurationOr(key string, backup time.Duration) time.Duration {
	return or(parse.Duration, key, backup)
}

// TimeRFC3339Or gets the environment variable as a RFC3339 time.Time.
func TimeRFC3339OrDefault(key string, backup time.Time) time.Time {
	return or(parse.TimeRFC3339, key, backup)
}

// Int8Or is an alias for MustParse(parse.Int, key).
func Int8Or(key string, fallback int8) int8 { return ParseOr(parse.Int8, key, fallback) }

// Int16Or is an alias for MustParse(parse.Int, key).
func Int16Or(key string, fallback int16) int16 { return or(parse.Int16, key, fallback) }

// Int32Or is an alias for MustParse(parse.Int, key).
func Int32Or(key string, fallback int32) int32 { return or(parse.Int32, key, fallback) }

// Int64Or is an alias for MustParse(parse.Int, key).
func Int64Or(key string, fallback int64) int64 { return or(parse.Int64, key, fallback) }

// IntOr is an alias for MustParse(parse.Int, key).
func IntOr(key string, fallback int) int { return or(parse.Int, key, fallback) }

func MustUint16(key string) uint16 { return must(parse.Uint16, key) }
func MustUint32(key string) uint32 { return must(parse.Uint32, key) }
func MustUint64(key string) uint64 { return must(parse.Uint64, key) }
func MustUint8(key string) uint8   { return must(parse.Uint8, key) }
func MustUInt(key string) uint     { return must(parse.Uint, key) }
