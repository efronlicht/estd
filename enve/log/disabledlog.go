//go:build estd_env_logdisabled && !estd_env_logcustom && !estd_env_logzap && !estd_env_logzero

package log

// LogMust does nothing.  See the build constraints enve_logdisabled and enve_logcustom.
func Must[T any](key string, err error, parser func(s string) (T, error)) {
	// intentionally empty.
}

// LogLookup does nothing.  See the build constraints enve_logdisabled and enve_logcustom.
func Lookup[T any](key string, err error, parser func(s string) (T, error)) {
	// intentionally empty.
}

// LogOr does nothing. See the build constraints enve_logdisabled and enve_logcustom.
func Or[T any](key string, err error, parser func(s string) (T, error), backup T) {
	// intentionally empty.
}
