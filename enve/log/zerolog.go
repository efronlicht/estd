//go:build estd_enve_logzero && !estd_env_logdisabled && !estd_env_logcustom && !estd_enve_logzero

package log

import (
	"reflect"
	"runtime"

	"github.com/rs/zerolog"
)

// Must is zerolog's hook for the Must group of functions (enve.MustInt, enve.MustPort, etc), enabled via the enve_logzero build tag. See the README for more details.
func Must[T any](key string, err error, parser func(s string) (T, error)) {
	if !shouldLog(key) { // at most once per key
		return
	}
	const msg = "missing or invalid environment variable"
	logIt(zerolog.DefaultContextLogger.Panic(), msg, key, err, parser)
}

// Lookup is zerolog's hook for the Lookup group of functions, enabled via the estd_enve_logzero build tag. See the README for more details.
func Lookup[T any](key string, err error, parser func(s string) (T, error)) {
	if !shouldLog(key) { // at most once per key
		return
	}
	const msg = "missing or invalid environment variable"
	logIt(zerolog.DefaultContextLogger.Info(), msg, key, err, parser)
}

// Or is zerolog's hook for the (enve.IntOr, enve.PortOr, etc), enabled via the enve_logzero build tag. See the README for more details.
func Or[T any](key string, err error, parser func(s string) (T, error), backup T) {
	if !shouldLog(key) { // at most once per key
		return
	}
	const msg = "missing or invalid environment variable: falling back to default"
	logIt(zerolog.DefaultContextLogger.Info().Interface("default", backup), msg, key, err, parser)
}

var _ zerolog.LogObjectMarshaler = _Meta{}

func (m _Meta) MarshalZerologObject(e *zerolog.Event) {
	e.Str("name", m.Name).Str("file", m.File).Int("line", m.Line)
}

func logIt[T any](evt *zerolog.Event, msg, key string, err error, parser func(s string) (T, error)) {
	evt.Err(err).
		Object("caller", callerMeta(1)).Object("parser", parserMeta(parser)).
		Str("key", key).
		Stringer("type", reflect.TypeOf((*T)(nil)).Elem()).
		Msg(msg)
}
