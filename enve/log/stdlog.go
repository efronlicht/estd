//go:build !estd_env_logdisabled && !estd_env_logcustom && !estd_enve_logzap && !estd_enve_logzero

package log

import (
	"log"

	"gitlab.com/efronlicht/estd/ereflect"
)

// Must is the default log hook for the 'Lookup' group of functions, using the log package in the stdlib. See the package README for details on
// other logging options (disabling, zap, zerolog, etc).
func Must[T any](key string, err error, parser func(s string) (T, error)) {
	if !shouldLog(key) { // at most once per key
		return
	}
	type_ := ereflect.TypeOf[T]()
	// no need to use the pool, since this will panic
	log.Printf("estd/enve/parse.Must[%s] caller: %s, parser %s: FATAL ERR: %v", type_, callerMeta(0), parserMeta(parser), err)
}

// Lookup is the default log hook for the 'Lookup' group of functions. See the package README for details on
// other logging options (disabling, zap, zerolog, etc).
func Lookup[T any](key string, err error, parser func(s string) (T, error)) {
	if !shouldLog(key) { // at most once per key
		return
	}

	log.Printf("estd/enve/parse.Lookup[%s]: caller %s, parser %s: err: %v", ereflect.TypeOf[T](), callerMeta(0), parserMeta(parser), err)
}

// Or is the default log hook for the 'Lookup' group of functions, using the log package in the stdlib.See the package README for details on
// other logging options (disabling, zap, zerolog, etc).
func Or[T any](key string, err error, parser func(s string) (T, error), backup T) {
	if !shouldLog(key) { // at most once per key
		return
	}
	log.Printf("estd/enve/parse.Or[%s]: caller %s, parser %s: err: %v: falling back to default (%+v )", ereflect.TypeOf[T](), callerMeta(0), parserMeta(parser), err, backup)
}
