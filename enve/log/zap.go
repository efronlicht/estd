//go:build estd_enve_logzap && !enve_logzero && !enve_logdisabled && !enve_logcustom

package log

import (
	"reflect"
	"runtime"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Must is zap's hook for the Must group of functions (enve.MustInt, enve.MustPort, etc), enabled via the enve_logzap build tag. See the README for more details.
func Must[T any](key string, err error, parser func(s string) (T, error)) {
	if !shouldLog(key) { // at most once per key
		return
	}
	pc, file, line, _ := runtime.Caller(skip) // not LogErr[T], and not the closure in LookupFromFunc[T], and not LookupFromFunc[T], but _it's_ caller.
	zap.L().Panic("missing or invalid environment variable",
		zap.Error(err),
		zap.Object("parser", parserMeta(parser)),
		zap.Object("caller", callerMeta()),
		zap.Stringer("type", reflect.TypeOf(*new(T))),
	)
}

func (m _Meta) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	oe.AddString("name", m.Name)
	oe.AddString("file", m.File)
	oe.AddInt("line", m.Line)
	return nil
}

// Lookup is zap's hook for the Lookup group of functions, enabled via the estd_enve_logzap build tag. See the README for more details.
func Lookup[T any](key string, err error, parser func(s string) (T, error)) {
	if !shouldLog(key) { // at most once per key
		return
	}
	pc, file, line, _ := runtime.Caller(skip) // not LogErr[T], and not the closure in LookupFromFunc[T], and not LookupFromFunc[T], but _it's_ caller.
	zap.L().Info("missing or invalid environment variable",
		zap.Error(err),
		zap.Object("parser", parserMeta(parser)),
		zap.Object("caller", callerMeta()),
		zap.Stringer("dst_type", typeOf[T]()),
	)
}

func typeOf[T any]() reflect.Type { return reflect.TypeOf((*T)(nil)).Elem() }

// Or is zap's hook for the (enve.IntOr, enve.PortOr, etc), enabled via the enve_logzap build tag. See the README for more details.
func Or[T any](key string, err error, parser func(s string) (T, error), backup T) {
	if !shouldLog(key) { // at most once per key
		return
	}
	pc, file, line, _ := runtime.Caller(skip) // not LogErr[T], and not the closure in LookupFromFunc[T], and not LookupFromFunc[T], but _it's_ caller.

	zap.L().Info("missing or invalid environment variable: falling back to default",
		zap.Error(err),
		zap.Object("parser", parserMeta(parser)),
		zap.Object("caller", callerMeta()),
		zap.Stringer("dst_type", typeOf[T]()),
		zap.Any("default", backup))
}
