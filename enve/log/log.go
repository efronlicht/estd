//go:build !enve_logdisabled

// shared logic between stdlog, zerolog, customlog
package log

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"runtime"
	"strconv"
	"sync"

	"gitlab.com/efronlicht/estd/eruntime"
)

var (
	seen     = make(map[string]bool)
	mux      sync.RWMutex
	once     sync.Once
	disabled bool
)

func shouldLog(key string) bool {
	once.Do(func() { // once ever
		disabled, _ = strconv.ParseBool(os.Getenv("ENVE_LOGDISABLED"))
		if disabled {
			return //
		}
		log.Printf("enve: standard logger enabled. disable with build flag enve_logdisabled, environment variable ENVE_LOGDISABLED, or use a custom logger with build tag enve_logcustom")
	})
	if disabled {
		return false
	}
	// check once: has anyone else logged this?
	mux.RLock()
	ok := seen[key]
	mux.RUnlock()
	if ok { //  they have.
		return false
	}
	// obtain the write lock.
	mux.Lock()
	defer mux.Unlock()
	// someone else might have got the write lock between when we released the read lock and obtained the write lock.
	if seen[key] { // they have; we have nothing to do.
		return false
	}
	// we're the first to obtain the write lock.
	seen[key] = true
	return true
}

type _Meta struct {
	Name, File string
	Line       int
}

func (m _Meta) String() string {
	return fmt.Sprintf("%s (%s %d)", m.Name, m.File, m.Line)
}

func callerMeta(extraSkip int) _Meta {
	pc, callerFile, callerLine, _ := runtime.Caller(skip + extraSkip)
	return _Meta{
		Name: trim(runtime.FuncForPC(pc).Name()),
		File: trim(callerFile),
		Line: callerLine,
	}
}

// TrimFunction trims a function path containing "efronlicht/estd", making it start with "estd".
// EG, "users/efron/go/src/gitlab.com/efronlicht/estd/parse/IPv4" => "estd/parse.IPv4".
// this looks nice, but has a runtime cost.
// use the build tag "eruntime_notrim" to disable it
var trim = eruntime.TrimFunction

func parserMeta[T any](f func(string) (T, error)) _Meta {
	meta := runtime.FuncForPC(reflect.ValueOf(f).Pointer())
	parserFile, parserLine := meta.FileLine(meta.Entry())
	return _Meta{
		Name: trim(meta.Name()),
		File: trim(parserFile),
		Line: parserLine,
	}
}

// how far up the stack to go to get caller information.
// supose we call env.DurationOr("CLIENT_TIMEOUT", 5*time.Second) from main.main().
// stack should look like this:
//
//	 callerMeta() // 0
//		log.Or[time.Duration]("CLIENT_TIMEOUT", ErrMissingKey("CLIENT_TIMEOUT", parse.Duration, 5*time.Second)) // 1
//		env.or[time.Duration](parse.Duration, "CLIENT_TIMEOUT", 5*time.Second) // 2
//		env.DurationOr("CLIENT_TIMEOUT", ErrMissingKey("CLIENT_TIMEOUT")) // 3
//		main.main() // 4 <<- this is what we want
const skip = 4
