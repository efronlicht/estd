package emath

import (
	"fmt"
	"math"

	"golang.org/x/exp/constraints"
)

// Sign returns the sign: -1 if n < 0, 1 if n > 0, 0 otherwise.
func Sign[N constraints.Signed](n N) N {
	switch {
	case n < 0:
		return -1
	case n > 0:
		return 1
	default:
		return 0
	}
}

// Min of a and b.
//
//	fmt.Println(-1 == Min(int64(2), int64(-1)))
//	//true
func Min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

// Max of a and b.
//
//	fmt.Println(2 == Max(int64(2), int64(-1)))
//	//true
func Max[T constraints.Ordered](a, b T) T {
	if a > b {
		return a
	}
	return b
}

// DefaultRelTol is the default relative tolerance for Close().
const DefaultRelTol = 1e-9

// DefaultAbsTol is zero.
const DefaultAbsTol = 0.0

// FloatTol represents the acceptable tolerance for float equality.
type FloatTol struct {
	// Relative and Absolute Tolerance
	Rel, Abs float64
}

// Close checks whether two floats are appromixately equal within 1e-9 relative tolerance (about 9 decimal places.)
// Identical to WithinTol(a, b, FloatTol{Rel: DefaultRelTol})
func Close[T constraints.Float](a, b T) bool {
	return WithinTol(a, b, FloatTol{Abs: DefaultAbsTol, Rel: DefaultRelTol})
}

// WithinTol checks whether two floating point numbers are approximately equal.
// The logic is taken from https://docs.python.org/3/library/math.html#math.isclose, the documentation of which I reproduce here.
/*
	`
	Whether or not two values are considered close is determined according to given absolute and relative tolerances.
	rel_tol is the relative tolerance – it is the maximum allowed difference between a and b, relative to the larger absolute value of a or b. For example, to set a tolerance of 5%, pass rel_tol=0.05. The default tolerance is 1e-09, which assures that the two values are the same within about 9 decimal digits. rel_tol must be greater than zero.
	abs_tol is the minimum absolute tolerance – useful for comparisons near zero. abs_tol must be at least zero.
	If no errors occur, the result will be: abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol).
	The IEEE 754 special values of NaN, inf, and -inf will be handled according to IEEE rules. Specifically, NaN is not considered close to any other value, including NaN. inf and -inf are only considered close to themselves.
	`
*/
func WithinTol[T constraints.Float](a, b T, tol FloatTol) bool {
	switch a, b := float64(a), float64(b); {
	case tol.Abs < 0, math.IsInf(tol.Abs, 0), math.IsNaN(tol.Abs), tol.Rel <= 0, math.IsInf(tol.Rel, 0), math.IsNaN(tol.Rel):
		panic(fmt.Sprintf("expected a %T where Absolute >= 0 and Relative > 0, but Absolute=%v, Relative=%v", tol, tol.Abs, tol.Rel))
	case math.IsInf(a, 1):
		return math.IsInf(b, 1)
	case math.IsInf(a, -1):
		return math.IsInf(b, -1)
	case math.IsInf(b, 0), math.IsNaN(a), math.IsNaN(b):
		return false
	default:
		adjustedRelative := tol.Rel * Max(math.Abs(a), math.Abs(b))
		return math.Abs(a-b) <= Max(adjustedRelative, tol.Abs)
	}
}

// Pow raises x to the nth power via exponentiation-by-squaring.
//
//	fmt.Println(Pow(2, 3))
//	8
func Pow[T constraints.Integer](x T, n uint32) T {
	if n == 0 {
		return 1
	}

	y := T(1)
	for n > 1 {
		if n%2 == 0 {
			n /= 2
			x *= x
		} else {
			y *= x
			x *= x
			n = (n - 1) / 2
		}
	}
	return x * y
}

// Clamp the value to be within the closed interval [min, max].
//
//	fmt.Println(Clamp[int](-5, 0.0, 5))
//	>> 0
//	fmt.Println(Clamp[f64](6.0, 0.0, 5.0)))
//	>> 5.0
func Clamp[T constraints.Ordered](n, min, max T) T {
	switch {
	case n < min:
		return min
	case n > max:
		return max
	default:
		return n
	}
}
