package emath_test

import (
	"sync"
	"testing"

	. "gitlab.com/efronlicht/estd/emath"
	"golang.org/x/exp/constraints"
)

func assertEqual[T comparable](t *testing.T, a, b T) {
	if a != b {
		t.Errorf("%s: assertEqual[%T]: expected '%v' but got '%v'", t.Name(), a, a, b)
	}
}

func TestMax(t *testing.T) {
	assertEqual(t, Max(2, 3), 3)
	assertEqual(t, Max("a", "cas"), "cas")
}

func TestMin(t *testing.T) {
	assertEqual(t, Min(2, 3), 2)
	assertEqual(t, Min("a", "cas"), "a")
}

func TestClamp(t *testing.T) {
	assertEqual(t, Clamp(1, 0, 10), 1)
	assertEqual(t, Clamp(-1, 0, 1), 0)
	assertEqual(t, Clamp(5, 0, 2), 2)
}

func TestSign(t *testing.T) {
	assertEqual(t, Sign(-200), -1)
	assertEqual(t, Sign(0), 0)
	assertEqual(t, Sign(20), 1)
}

func slowPow[T constraints.Integer](base T, exp uint32) T {
	n := T(1)
	for ; exp > 0; exp-- {
		n *= base
	}
	return n
}

func TestPow(t *testing.T) {
	if testing.Short() {
		t.Skipf(t.Name(), "tests entire int16 range")
	}
	t.Parallel()
	wg := new(sync.WaitGroup)
	wg.Add(0xFFFF)
	for n := 0; n < 0xFF; n++ {
		for m := uint32(0); m <= 0xFF; m++ {
			assertEqual(t, Pow(n, m), slowPow(n, m))
			assertEqual(t, Pow(uint8(n), m), slowPow(uint8(n), m))
			assertEqual(t, Pow(int64(n), m), slowPow(int64(n), m))
		}
	}
}
