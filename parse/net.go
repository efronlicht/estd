// net covers the types in net.
package parse

import (
	"errors"
	"fmt"
	"net"
	"strconv"
)

//	// wrapper for net.ParseIP
//
// IP parses the string as a net.IP.
// The following forms are OK:
//
//	IPv4 dotted decimal ("192.0.2.1")
//	IPv4-mapped IPv6 ("::ffff:192.0.2.1")
//	IPv6 ("2001:db8::68")
func IP(s string) (net.IP, error) {
	ip := net.ParseIP(s)
	if ip == nil {
		return nil, &Error[net.IP]{Raw: s, Err: errors.New("not a valid IPv4 or ipv6")}
	}
	return ip, nil
}

// IPv4 parses the string as an IPv4.
// The following forms are OK:
//
//	IPv4 dotted decimal ("192.0.2.1")
//	IPv4-mapped IPv6 ("::ffff:192.0.2.1")
func IPv4(s string) (net.IP, error) {
	ip := net.ParseIP(s).To4()
	if ip == nil {
		return nil, &Error[net.IP]{Raw: s, Err: errors.New("not a valid IPv4")}
	}
	return ip, nil
}

// Port parses a port, with or without the leading ":".
func Port(s string) (uint16, error) {
	if s == "" {
		return 0, &Error[uint16]{Raw: "", Err: errors.New("expected a valid port, but got the empty string")}
	}
	if s[0] == ':' { // trim the leading ':', if necessary.
		s = s[1:]
	}
	n, err := strconv.ParseUint(s, 0, 16)
	if err != nil {
		return 0, &Error[uint16]{Raw: s, Err: fmt.Errorf("expected a valid port: %w", err)}
	}
	return uint16(n), nil
}

//	// wrapper for net.ParseMAC
//
// MAC parses the string as a net.HardwareAddr
// ParseMAC parses s as an IEEE 802 MAC-48, EUI-48, EUI-64, or a 20-octet IP over InfiniBand link-layer address using one of the following formats:
//
// 00:00:5e:00:53:01
// 02:00:5e:10:00:00:00:01
// 00:00:00:00:fe:80:00:00:00:00:00:00:02:00:5e:10:00:00:00:01
// 00-00-5e-00-53-01
// 02-00-5e-10-00-00-00-01
// 00-00-00-00-fe-80-00-00-00-00-00-00-02-00-5e-10-00-00-00-01
// 0000.5e00.5301
// 0200.5e10.0000.0001
// 0000.0000.fe80.0000.0000.0000.0200.5e10.0000.0001
func MAC(s string) (net.HardwareAddr, error) {
	addr, err := net.ParseMAC(s)
	if err != nil {
		return nil, &Error[net.HardwareAddr]{Raw: s, Err: errors.New("not a valid hardware addr")}
	}
	return addr, nil
}
