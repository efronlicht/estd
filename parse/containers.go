package parse

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/efronlicht/estd/containers/set"
)

// HSet parses text as a series of keys separated by whitespace and/or commas. The rules are identical to Slice[K]. Duplicate keys are NOT an error.
func HSet[K comparable](
	parse func(string) (K, error),
	raw string,
) (set.HSet[K], error) {
	a, err := Slice(parse, raw)
	if err != nil {
		return nil, err
	}
	s := make(set.HSet[K], len(a))
	for _, k := range a {
		s[k] = struct{}{}
	}
	return s, nil
}

// MapFunc(parseK, parseV) creates a closure f such that f(s) = Map(parseK, parseV, s)
func MapFunc[K comparable, V any](
	parseK func(string) (K, error),
	parseV func(string) (V, error),
) func(s string) (map[K]V, error) {
	return func(s string) (map[K]V, error) { return Map(parseK, parseV, s) }
}

// KeyVal parses a 'sep'-separated key-value pair k<SEP>v by calling fk on k and fv on v.
// eg, KeyVal(`"expires":10m", ':', strconv.Unquote, ParseDuration) should return ("expires", 10*time.Minute, nil)
// You probably don't want to use this directly unless you're implementing parsing of a data-structure directly.
func KeyVal[K any, V any](
	kv string,
	sep byte,
	parseKey func(string) (K, error),
	parseVal func(string) (V, error),
) (k K, v V, err error) {
	rawK, rawV, ok := splitOnce(kv, sep)
	if !ok {
		return k, v, errors.New("missing ':'")
	}
	if k, err = parseKey(strings.TrimSpace(rawK)); err != nil {
		return k, v, fmt.Errorf("failed to parse key: %w", err)
	}
	if v, err = parseVal(strings.TrimSpace(rawV)); err != nil {
		return k, v, fmt.Errorf("failed to parse val: %w", err)
	}
	return k, v, nil
}

// Map parses a string in the form "{k0:v0, k1:v1}" by calling parseK on the keys and parseV on the vals.
// The map must have unique keys: duplicate fields are an error.
// Non-nil errors are of type *MapError.
func Map[K comparable, V any](
	parseK func(rawKey string) (K, error),
	parseV func(rawVal string) (V, error),
	raw string,
) (map[K]V, error) {
	kvs := fields(raw)
	m := make(map[K]V, len(kvs))
	for i, kv := range kvs {
		k, v, err := KeyVal(kv, ':', parseK, parseV)
		if err != nil {
			return nil, &MapError[K, V]{Raw: raw, Err: err, I: i, N: len(kvs), Sub: kv}
		}
		if _, ok := m[k]; ok {
			return nil, &MapError[K, V]{Raw: raw, Err: fmt.Errorf("duplicate key %v", k), I: i, N: len(kvs), Sub: kv}
		}
		m[k] = v
	}
	return m, nil
}

// Slice parses text as a series of values
// separated by commas (and optional whitespace), optionally enclosed in a [] or {}.
// An error will be of type *ParseError[T].
func Slice[T any](parse func(s string) (T, error), s string) (a []T, err error) {
	f := fields(s)
	a = make([]T, len(f))
	for i := range a {
		if a[i], err = parse(strings.TrimSpace(f[i])); err != nil {
			return nil, &SliceError[T]{Raw: s, Err: err, I: i, Sub: f[i]}
		}
	}
	return a, nil
}

// SliceFunc returns a closure f such that f(s) = Slice(parse, s).
func SliceFunc[T any](parse func(s string) (T, error)) func(string) ([]T, error) {
	return func(s string) (a []T, err error) {
		return Slice(parse, s)
	}
}

func splitOnce(s string, b byte) (string, string, bool) {
	i := strings.IndexByte(s, b)
	if i == -1 {
		return "", "", false
	}
	return s[:i], s[i+1:], true
}

func fields(s string) []string {
	// exactly one enclosing '[' and ']' is OK: trim it.
	if len(s) >= 2 {
		if f, b := s[0], s[len(s)-1]; f == '[' && b == ']' || f == '{' && b == '}' {
			s = s[1 : len(s)-1]
		}
	}
	return strings.Split(strings.TrimSpace(s), ",")
}
