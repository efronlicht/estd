package parse

// primitives contains parsers for go primitives.
// these are mostly just wrappers around the functions in strconv.
import (
	"strconv"
)

// Complex128 wraps strconv.ParseComplex(s, 128)
func Complex128(s string) (c complex128, err error) {
	c, err = strconv.ParseComplex(s, 128)
	if err != nil {
		return c, &Error[complex128]{Raw: s, Err: err}
	}
	return c, nil
}

// Bool wraps strconv.ParseBool. It accepts 1, t, T, TRUE, true, True, 0, f, F, FALSE, false, False. Any other value returns an error.
func Bool(s string) (b bool, err error) {
	b, err = strconv.ParseBool(s)
	if err != nil {
		return b, &Error[bool]{Raw: s, Err: err}
	}
	return b, nil
}

// Float64 wraps strconv.ParseFloat(s, 64).
func Float64(s string) (x float64, err error) {
	x, err = strconv.ParseFloat(s, 64)
	if err != nil {
		return x, &Error[float64]{Raw: s, Err: err}
	}
	return x, nil
}
