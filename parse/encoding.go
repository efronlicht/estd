package parse

// encoding covers unmarshaling from types that implement the various interfaces
// in the stdlib encoding package.

import (
	"encoding"
	"encoding/json"
	"fmt"
)

// FromJSON attempts to unmarshal s to a *T using json.Unmarshal([]byte(s), &t)
func FromJSONString[T any](s string) (t T, err error) {
	if err := json.Unmarshal([]byte(s), &t); err != nil {
		return t, &Error[T]{Raw: s, Err: fmt.Errorf("failed to unmarshal JSON into a %T: %w", &t, err)}
	}
	return t, nil
}

// FromBinary parses an encoding.BinaryUnmarshaler from a []byte by using its UnmarshalBytes() method.
func FromBinary[T any, PT interface {
	*T
	encoding.BinaryUnmarshaler
}](b []byte,
) (T, error) {
	pt := PT(new(T))
	err := pt.UnmarshalBinary(b)
	if err != nil {
		return *pt, &Error[T]{Raw: string(b), Err: err}
	}
	return *pt, err
}

// FromTextBytes parses an encoding.TextUnmarshaler from a []byte by using its UnmarshalText() method.
func FromTextBytes[T any, PT interface {
	*T
	encoding.TextUnmarshaler
}](b []byte,
) (T, error) {
	pt := PT(new(T))
	err := pt.UnmarshalText(b)
	if err != nil {
		return *pt, &Error[T]{Raw: string(b), Err: err}
	}
	return *pt, err
}

// FromTextBytes parses an encoding.TextUnmarshaler from a []byte by using its UnmarshalText() method.
func FromText[T any, PT interface {
	*T
	encoding.TextUnmarshaler
}](s string,
) (T, error) {
	pt := PT(new(T))
	err := pt.UnmarshalText([]byte(s))
	if err != nil {
		return *pt, &Error[T]{Raw: s, Err: err}
	}
	return *pt, err
}
