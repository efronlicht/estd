package parse

import (
	"fmt"
)

type (
	// SliceError is returned from a failed call to Slice(parse, s). It will Unwrap() to
	// the error of parse(), if any.
	SliceError[T any] struct {
		// The text that failed parsing.
		Raw string
		// Underlying error that caused the failure. Returned by Unwrap().
		Err error
		// Index of the comma-separated item where parsing failed, if any.
		I int
		// The item at index I.
		Sub string
	}
	// MapError is returned from a failed call to Slice(parseK, parseV, s). It will Unwrap() to
	// the error of parse(), if any.
	MapError[K comparable, V any] struct {
		// The text that failed parsing.
		Raw string
		// Underlying error that caused the failure. Returned by Unwrap().
		Err error
		// Index of the comma-separated item where parsing failed, if any.
		I int
		// Number of elements
		N int
		// The item at index I, in the form key:value
		Sub string
	}
	// Error is a generic parsing error, used for primitives.
	Error[T any] struct {
		Raw string
		Err error
	}
)

func (err *MapError[K, V]) Error() string {
	return fmt.Sprintf("failed to parse value %q as a %T: failed to parse item #%d/%d(%q): %s", err.Raw, map[K]V(nil), err.I, err.N, err.Sub, err.Err)
}

// Unwrap() returns the underlying error that caused the parsing failure.
func (err *MapError[K, V]) Unwrap() error { return err.Err }

func (err *Error[T]) Error() string {
	return fmt.Sprintf("parse error: %T: %v: string was %q", *new(T), err.Err, err.Raw)
}

// Unwrap() returns the underlying error that caused the parsing failure.
func (err *SliceError[T]) Unwrap() error { return err.Err }

// Unwrap() returns the underlying error that caused the parsing failure.
func (err *SliceError[T]) Error() string {
	return fmt.Sprintf("parsing a %T: failed on item %d: %v. raw text: %q", []T(nil), err.I, err.Err, err.Raw)
}

// Unwrap() returns the underlying error that caused the parsing failure.
func (err *Error[T]) Unwrap() error { return err.Err }
