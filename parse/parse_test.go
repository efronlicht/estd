package parse_test

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"gitlab.com/efronlicht/estd/equal"
	"gitlab.com/efronlicht/estd/parse"
)

type assertion[T comparable] struct {
	*testing.T
	f func(string) (T, error)
}

func (a assertion[T]) Error(s string) {
	got, err := a.f(s)
	if err == nil {
		a.Errorf("expected parse[%T](%s) to return an error", got, s)
	}
}

func (a assertion[T]) Equal(s string, want T) {
	a.OK(s)
	if got, _ := a.f(s); got != want {
		a.Errorf("expected parse[%T](%s)=%v, but got %v", got, s, want, got)
	}
}

func (a assertion[T]) OK(s string) {
	got, err := a.f(s)
	if err != nil {
		a.Errorf("expected parse[%T](%s) not to return an error, but got %v", got, s, err)
	}
}

func TestParseIP(t *testing.T) {
	v4 := "192.168.1.1"
	if _, err := parse.IPv4(v4); err != nil {
		t.Fatal(err)
	}
	const v6 = "2001:db8::8a2e:370:7334"
	if _, err := parse.IP(v6); err != nil {
		t.Fatal(err)
	}
	if _, err := parse.IPv4(v6); err == nil {
		t.Fatalf("expected an error: %s is not reducible to an ipv4", v6)
	}
}

func TestFromJSON(t *testing.T) {
	type Names = struct{ First, Middle, Last string }
	const JSON = `{"efron": {"First": "Efron", "Middle": "Amber", "Last": "Licht"}}`
	got, err := parse.FromJSONString[map[string]Names](JSON)
	want := map[string]Names{"efron": {"Efron", "Amber", "Licht"}}
	if err != nil {
		t.Fatal(err)
	}
	if !equal.Map(got, want) {
		t.Fatalf("expected %v, but got %v", want, got)
	}
	if _, err := parse.FromJSONString[map[int]Names](JSON); err == nil {
		t.Fatal("expected an error: wrong type")
	}
}

func TestParseBool(t *testing.T) {
	a := assertion[bool]{t, parse.Bool}
	for _, s := range []string{"T", "t", "true", "True", "TRUE", "1"} {
		a.Equal(s, true)
	}
	for _, s := range []string{"F", "f", "False", "false", "0"} {
		a.Equal(s, false)
	}
	for _, s := range []string{"asdnjalsd", "  "} {
		a.Error(s)
	}
}

func TestParseBase(t *testing.T) {
	if gotHex, err := parse.UintBase("ABCDEF", 16); err != nil || gotHex != 0xABCDEF {
		t.Fatal(err, gotHex)
	}

	if _, gotErr := parse.UintBase("FFFFFF", 8); gotErr == nil {
		t.Fatalf("expected a syntax error but got nil")
	}
	if gotOctal, gotErr := parse.UintBase("234", 8); gotErr != nil || gotOctal != 0o234 {
		t.Fatalf("expected a syntax error but got nil")
	}
	if gotHex, err := parse.IntBase("-FFFFFF", 16); err != nil || gotHex != -0xFFFF_FF {
		t.Fatal(err, gotHex, "-FFFF_FF")
	}
	if _, gotErr := parse.IntBase("-FFFFFF", 8); gotErr == nil {
		t.Fatalf("expected a syntax error but got nil")
	}
}

func TestParseTrace(t *testing.T) {
	t.Parallel()
	const label = "company/application/os/commit/pod"
	const uid = `612f2911-1d94-4d4f-94b0-4e163a83b32c`
	const date = `1985-04-12T23:00:00Z`
	for wantErr, s := range map[string]string{
		"empty label":  "!" + uid + "!" + date,
		"no '!'":       label + uid + date,
		"only one '!'": label + "!" + uid + date,
		"parse uuid":   label + label + "!" + "foo" + "!" + date,
		"parse time":   label + "foo" + "!" + uid + "!" + `1985-04-12T23:0:00`, // note: missing timezone
	} {
		wantErr, s := wantErr, s
		t.Run(wantErr, func(t *testing.T) {
			t.Parallel()
			t.Log(wantErr, len(s))
			if _, err := parse.TraceNode(s); err == nil || !strings.Contains(strings.ToLower(err.Error()), wantErr) {
				t.Errorf("parse.TraceNode(%s): expected an err containing %s, but got %s", s, wantErr, err)
			}
		})
	}
	const good = label + "!" + uid + "!" + date
	got, err := parse.TraceNode(good)

	switch {
	case err != nil:
		t.Error("expected no error, but got ", err)
	case got.Label != label, !strings.EqualFold(got.ID.String(), uid), got.Start.Format(time.RFC3339) != date:
		t.Errorf("expected label =%s, id = %s, time = %s, but got %+v", label, uid, date, got)
	}
	gotTrace, err := parse.Trace(fmt.Sprintf("[\n\t%s,\n\t%s]", good, good))
	switch {
	case err != nil:
		t.Error("expected no error, but got ", err)
	case gotTrace[0].Label != label, !strings.EqualFold(gotTrace[0].ID.String(), uid), gotTrace[0].Start.Format(time.RFC3339) != date:
		t.Errorf("expected label =%s, id = %s, time = %s, but got %+v", label, uid, date, got)
	}
}

func TestParseHardwareAddr(t *testing.T) {
	t.Parallel()
	for _, s := range strings.Fields(`00:00:5e:00:53:01
	02:00:5e:10:00:00:00:01
	00:00:00:00:fe:80:00:00:00:00:00:00:02:00:5e:10:00:00:00:01
	00-00-5e-00-53-01
	02-00-5e-10-00-00-00-01
	00-00-00-00-fe-80-00-00-00-00-00-00-02-00-5e-10-00-00-00-01
	0000.5e00.5301
	0200.5e10.0000.0001
	0000.0000.fe80.0000.0000.0000.0200.5e10.0000.0001`) {
		if _, err := parse.MAC(s); err != nil {
			t.Errorf("expected %s to be a valid hardware address", s)
		}
		collapsed := strings.NewReplacer(":", "", "-", "", ".", "").Replace(s)
		if _, err := parse.MAC(collapsed); err == nil {
			t.Errorf("expected %s not to be a valid hardware address", s)
		}

	}
}

func TestParsePort(t *testing.T) {
	t.Parallel()
	a := assertion[uint16]{t, parse.Port}
	for s, want := range map[string]uint16{"0xff": 0xff, "28": 28, ":8080": 8080} {
		a.Equal(s, want)
	}
	for name, s := range map[string]string{"empty": "", "negative": "-1", "not a number": "ajhslkd", "out of range for uint16": "0xffff_f"} {
		t.Log(name, s)
		a.Error(s)
	}
}
