package parse

import (
	"fmt"

	"gitlab.com/efronlicht/estd/trace"

	"github.com/google/uuid"
)

// UUID decodes s into a UUID or returns an error.
// The following forms are OK:
//
//	xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx  // standard
//	urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx  // also standard
//	{xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}  // microsoft
//	xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx. // raw hex
//
// This is just a wrapper for uuid.Parse.
func UUID(s string) (uuid.UUID, error) {
	uid, err := uuid.Parse(s)
	if err != nil {
		return uid, &Error[uuid.UUID]{Raw: s, Err: err}
	}
	return uid, nil
}

// Trace parses a slice of *trace.Node.
func Trace(s string) (trace.Trace, error) { return Slice(TraceNode, s) }

// TraceNode parses a *trace.Node from the form  "<label>!<id>!<time>"
func TraceNode(s string) (*trace.Node, error) {
	errf := func(format string, args ...any) error {
		return &Error[*trace.Node]{Raw: s, Err: fmt.Errorf(format, args...)}
	}

	label, s, ok := splitOnce(s, '!')
	if !ok {
		return nil, errf(`expected a string in the form "<label>!<id>!<time>": no '!'`)
	}
	if label == "" {
		return nil, errf(`expected a string in the form "<label>!<id>!<time>": empty label`)
	}
	// an empty label is invalid, but since UIDs and RFC3339 dates are of fixed length, we already know this is impossible.
	rawID, rawStart, ok := splitOnce(s, '!')
	if !ok {
		return nil, errf(`expected a string in the form "<label>!<id>!<time>": only one '!'`)
	}
	id, err := UUID(rawID)
	if err != nil {
		return nil, errf("failed to parse uuid: %w", err)
	}
	start, err := TimeRFC3339(rawStart)
	if err != nil {
		return nil, errf("failed to parse time: %w", err)
	}
	return &trace.Node{Label: label, ID: id, Start: start}, nil
}
