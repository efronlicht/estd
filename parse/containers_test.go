package parse

import (
	"strings"
	"testing"

	"gitlab.com/efronlicht/estd/containers/set"
)

func mapEqual[K, V comparable](a, b map[K]V) bool {
	if len(a) != len(b) {
		return false
	}
	for k := range a {
		if b[k] != a[k] {
			return false
		}
	}
	return true
}

func TestMapHappyPath(t *testing.T) {
	t.Parallel()
	want := map[int8]uint8{-1: 0, 1: 2}
	const s = `{-1: 0, 1: 2}`
	m, err := MapFunc(Int8, Uint8)(s)
	if err != nil {
		t.Fatal(err)
	}
	if !mapEqual(m, want) {
		t.Fatalf("expected %v, got %v", want, m)
	}
}

func TestHSet(t *testing.T) {
	t.Parallel()
	got, err := HSet(Int, "2, 3, 5,\n7,\t\t11,\t13")
	if err != nil {
		t.Fatalf("expected no error, but got %v", err)
	}
	want := make(set.HSet[int], 6).WithKeys(2, 3, 5, 7, 11, 13)
	if !got.Equal(want) {
		t.Fail()
	}
}

func TestMapErrors(t *testing.T) {
	t.Parallel()
	t.Run("bad key", func(t *testing.T) {
		const s = `{-9999: 0, 1: 2}` // -9999 out of range for i8
		_, err := Map(Int8, Uint8, s)
		if err == nil || !strings.Contains(err.Error(), "key") {
			t.Fatalf("expected an error containing \"key\", but got %s", err)
		}
	})
	t.Run("bad val", func(t *testing.T) {
		const s = `{-1: 0, 0: -1}` // -1 out of range for u8
		_, err := Map(Int8, Uint8, s)
		if err == nil || !strings.Contains(err.Error(), "val") {
			t.Fatalf("expected an error containing \"val\", but got %s", err)
		}
	})
	t.Run("duplicate key", func(t *testing.T) {
		const s = `{-1: 0, -1: 2}`
		_, err := Map(Int8, Uint8, s)
		if err == nil || !strings.Contains(err.Error(), "duplicate") {
			t.Fatalf("expected an error containing \"duplicate\", but got %s", err)
		}
	})
}
