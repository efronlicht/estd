// time covers the package 'time'.
package parse

import "time"

// ParseDuration parses a duration string.
// A duration string is a possibly signed sequence of decimal numbers, each with optional fraction and a unit suffix, such as "300ms", "-1.5h" or "2h45m".
// Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
func Duration(s string) (time.Duration, error) {
	d, err := time.ParseDuration(s)
	if err != nil {
		return 0, &Error[time.Duration]{Raw: s, Err: err}
	}
	return d, err
}

// TimeRFC3339 is as time.Parse(time.RFC3339, s).
func TimeRFC3339(s string) (time.Time, error) {
	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		return t, &Error[time.Duration]{Raw: s, Err: err}
	}
	return t, nil
}
