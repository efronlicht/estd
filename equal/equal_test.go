package equal

import "testing"

func todo(t *testing.T) {
	t.Helper()
	t.Fatal(t.Name(), ": TODO")
}
func ptrTo[T any](t T) *T { pt := &t; return pt }
func TestPointerRef(t *testing.T) {
	if !PointerRef(ptrTo(2), ptrTo(2)) || PointerRef(nil, ptrTo(2)) || PointerRef(ptrTo(2), ptrTo(3)) {
		t.Fail()
	}
}

func TestMap(t *testing.T) {
	if !Map(map[string]int{"foo": 2}, map[string]int{"foo": 2}) {
		t.Fail()
	}
	if Map(map[string]int{"foo": 2}, nil) {
		t.Fail()
	}
	if Map(map[string]int{"foo": 2}, map[string]int{"foo": 1}) {
		t.Fail()
	}
}

func TestSlice(t *testing.T) {
	switch {
	case !Slice([]int{2, 3, 4}, []int{2, 3, 4}),
		!Slice([]int{}, nil),
		Slice([]string{"foo", "bar"}, []string{"foo", "baz"}),
		Slice([]int{3}, nil):
		t.Fail()
	}
}

func TestSliceF(t *testing.T) {
	absEq := func(a int, b int) bool {
		if a < 0 {
			a = -a
		}
		if b < 0 {
			b = -b
		}
		return a == b
	}
	if !SliceF([]int{2, 3, 4}, []int{2, -3, 4}, absEq) || SliceF([]int{2}, []int{-3}, absEq) {
		t.Fail()
	}
}
