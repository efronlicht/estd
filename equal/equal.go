package equal

// PointerRef: (a == nil && b == nil) || (*a == *b)
func PointerRef[T comparable](a, b *T) bool {
	return (a == nil && b == nil) || (a != nil && b != nil && *a == *b)
}

func id[T comparable](a, b T) bool { return a == b }

// Map a and b are equal if they contain exactly the same keys and values.
func Map[K, V comparable](a, b map[K]V) bool { return MapF(a, b, id[V]) }

// Slice a and b are equal when len(a) == len(b) && ∀ i<len(a), a[i]==b[i].
func Slice[T comparable](a, b []T) bool {
	return SliceF(a, b, id[T])
}

// SliceF a and b are equal when len(a) == len(b) && ∀ i<len(a), F(a[i], b[i]) == true
func SliceF[T any](a, b []T, f func(T, T) bool) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if !f(a[i], b[i]) {
			return false
		}
	}
	return true
}

// MapF a and b are equal when len(a) == len(b), they share the same keys, and for each key k,
// f(a[k], b[k]) is == true.
func MapF[K comparable, V any](a, b map[K]V, f func(V, V) bool) bool {
	if len(a) != len(b) {
		return false
	}
	for k, va := range a {
		if vb, ok := b[k]; !ok || !f(va, vb) {
			return false
		}
	}
	return true
}
