package test

import (
	"fmt"
	"testing"
)

func SkipLongf(t *testing.T, format string, args ...any) {
	if testing.Short() {
		t.Skipf(fmt.Sprintf("SKIP %s: ", t.Name())+format, args...)
	}
}
