//go:build !estd_nozaplog && !estd_nozerolog && !estd_nodeps

package trace

import (
	"gitlab.com/efronlicht/estd/zaputil"
	"go.uber.org/zap/zapcore"
)

var (
	_ zapcore.ArrayMarshaler  = Trace(nil)
	_ zapcore.ObjectMarshaler = (*Node)(nil)
)

func (t Trace) MarshalLogArray(ae zapcore.ArrayEncoder) error {
	return zaputil.OArray[*Node](t).MarshalLogArray(ae)
}

func (n *Node) MarshalLogObject(oe zapcore.ObjectEncoder) error {
	oe.AddString("id", n.ID.String())
	oe.AddString("label", n.Label)
	oe.AddTime("start", n.Start)
	return nil
}
