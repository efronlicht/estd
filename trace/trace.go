// Package trace tracks execution across function (and machine) bounds.
// Build a trace with New(), and add nodes via With() or WithCaller().
// Easily serialize and deserialize a Trace with String() and parse.Trace.
package trace

import (
	"errors"
	"fmt"
	"io"
	"time"
	"unicode"

	"gitlab.com/efronlicht/estd/eruntime"

	"github.com/google/uuid"
)

// Trace execution across function (and machine) bounds by building a trace with New() and adding nodes at points of interest via With() or WithCaller().
type Trace []*Node

var _, _ io.WriterTo = Trace(nil), (*Node)(nil)

// New returns a nil trace, ready for use.
func New() Trace { return nil }

// Extend the trace with a new node, returning a copy.
func (t Trace) Extend(label string) Trace {
	dst := make(Trace, len(t))
	copy(dst, t)
	return append(dst, NewNode(label))
}

// Node is an individual point in a trace. Build with NewNode().
type Node struct {
	// Label is a user-defined tag. A label can't contain whitespace (see unicode.IsSpace) or the character '!'.
	Label string
	// ID is randomly generated as a uuidv4 when NewNode() is called.
	ID uuid.UUID
	// Start is
	Start time.Time
}

// String formats a node as <label>!<id>!<start>
func (n *Node) String() string {
	return fmt.Sprintf("%s!%s!%s", n.Label, n.ID, n.Start.Format(time.RFC3339))
}

func (t Trace) WithCaller(skip int) Trace {
	return t.With(eruntime.Caller(skip + 1).String())
}

// CheckLabel validates a label. A valid label is:
//   - non-empty
//   - contains no spaces (as unicode.IsSpace)
//   - does not contain the rune '!'.
func CheckLabel(label string) error {
	if label == "" {
		return errors.New("empty")
	}
	for i, r := range label {
		if unicode.IsSpace(r) {
			return fmt.Errorf("unicode space '%c' (%02x) at offset %d", i, r, r)
		}
		if r == '!' {
			return fmt.Errorf("separator character '!' at offset %d", i)
		}
	}
	return nil
}

// NewNode makes a new node from the label.
// A label can't be empty, contain whitespace, or the character '!'.
// Such characters will be replaced with their escape: eg, ' ' => "\0x20"
// Such a label will panic.
// Use CheckLabel(label) first if you're not sure.
func NewNode(label string) *Node {
	if err := CheckLabel(label); err != nil {
		panic(fmt.Errorf("invalid label %q: %w", label, err))
	}
	return &Node{Label: label, ID: uuid.New(), Start: time.Now()}
}

func (t Trace) With(label string) Trace {
	return append(t, NewNode(label))
}
