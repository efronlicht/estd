package trace

import (
	"fmt"
	"io"
	"time"

	"gitlab.com/efronlicht/estd/containers/esync"
)

func i64(n int, err error) (int64, error) { return int64(n), err }

// WriteTo the provided writer as a pretty-printed trace.
func (t Trace) WriteTo(w io.Writer) (n int64, err error) {
	write := func(s string) (int64, error) {
		m, err := w.Write([]byte(s))
		return int64(m), err
	}
	if len(t) == 0 {
		return write("[]")
	}
	if len(t) == 1 {
		return i64(fmt.Fprintf(w, "[%s]", t[0]))
	}
	m, err := write("[")
	n += m
	if err != nil {
		return n, err
	}

	m, err = write("\n\t")
	n += m
	if err != nil {
		return n, err
	}
	for i := range t[:len(t)-1] {
		m, err = write(",\n\t")
		n += m
		if err != nil {
			return n, err
		}
		m, err := t[i].WriteTo(w)
		n += m
		if err != nil {
			return n, err
		}
	}
	m, err = i64(w.Write([]byte("\n\t")))
	n += m
	return m, err
}

func (n *Node) WriteTo(w io.Writer) (int64, error) {
	m, err := fmt.Fprintf(w, "%s!%s!%s", n.Label, n.ID, n.Start.Format(time.RFC3339))
	return int64(m), err
}

func (t Trace) String() string {
	buf := esync.BufPool.Get()
	buf.Reset()
	defer func() {
		buf.Reset()
		esync.BufPool.Put(buf)
	}()
	_, _ = t.WriteTo(buf)
	return buf.String()
}
