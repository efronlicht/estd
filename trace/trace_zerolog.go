//go:build !estd_nozerolog && !estd_nodeps

package trace

import "github.com/rs/zerolog"

var (
	_ zerolog.LogArrayMarshaler  = Trace(nil)
	_ zerolog.LogObjectMarshaler = (*Node)(nil)
)

func (t Trace) MarshalZerologArray(a *zerolog.Array) {
	for i := range t {
		a.Object(t[i])
	}
}

func (n *Node) MarshalZerologObject(e *zerolog.Event) {
	e.Str("id", n.ID.String()).Str("label", n.Label).Time("start", n.Start)
}
